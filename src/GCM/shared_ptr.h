/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   A shared pointer implementation relying on an Allocator object.
 *
 ******************************************************************************
 */

#ifndef GCM_SHARED_PTR_H_
#define GCM_SHARED_PTR_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <utility>

/* User */
#include "mem_ref.h"


namespace GCM
{

/* Forward declarations */
template <typename T> class WeakPtr;

/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

/**
 * \brief A shared pointer to an object managed by an Allocator.
 *
 * Shared pointers own a shared reference to an object. This reference has
 * a counter corresponding to the number of shared pointers currently owning
 * the reference.
 *
 * Unlike regular smart pointers, when the last shared pointer owning the
 * reference to an object is destroyed, the object is not destroyed and
 * deallocated right away. This is delayed until a garbage collection is
 * triggered (Allocator::GarbageCollection()).
 *
 * Valid shared pointers can only be created using Allocator::New() or by
 * copying or moving another valid shared pointer.
 */
template <typename T>
class SharedPtr
{
public:
    /* Public constants */

    /* Public types */

    /* Public static methods */

    /* Public methods */
    /**
     * \brief Returns the current number of references on the owned object.
     */
    unsigned int RefCount() const;

    /**
     * \brief Release the current reference, making the shared pointer empty
     * as if default-constructed.
     */
    void Release();

    /* Getters, setters */

    /* Constructors, destructor */
    /**
     * \brief Construct an empty shared pointer, owning no reference.
     *
     * Such an object shall only be assigned another shared pointer
     * or destroyed.
     */
	SharedPtr() noexcept
        : ref_(nullptr)
    {}

    /**
     * \brief Initialise the shared pointer with \a other's object
     * reference, increasing the reference count.
     */
    SharedPtr(const SharedPtr<T> &other) noexcept;

    /**
     * \brief Initialise the shared pointer by acquiring \a other's object
     * reference.
     *
     * \a other becomes invalid, and the reference count is not modified.
     */
    SharedPtr(SharedPtr<T> &&other) noexcept;

	~SharedPtr();

    /* Operators */
    // Shall only be used if the shared pointer is valid
    T& operator*() const;
    T* operator->() const;

    // Returns true if the shared pointer is valid
    explicit operator bool() const noexcept;

    SharedPtr<T>& operator=(const SharedPtr<T> &other) noexcept;

    /**
     * \brief Move-assign \a other to this object.
     *
     * The counter of the new reference is not increased.
     *
     * \warning After this operation, \a other is empty and shall only be
     * assigned another shared pointer or destroyed.
     */
    SharedPtr<T>& operator=(SharedPtr<T> &&other) noexcept;

protected:
    /* Protected constants */

    /* Protected types */

    /* Protected methods */
	// For the allocator to construct a shared pointer
	SharedPtr(MemRefInfo *ref)
		: ref_(ref)
	{}

    /**
     * \brief Decrement ref counter.
     *
     * If the counter drops to 0, also signal there is uncollected garbage.
     */
    void decCounter();

    /* Protected attributes */
	MemRefInfo *ref_;

	friend class Allocator;
    friend class WeakPtr<T>;

    friend void swap(SharedPtr<T> &lhs, SharedPtr<T> &rhs)
    {
        std::swap(lhs.ref_, rhs.ref_);
    }
};


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace

// SharedPtr implementation
#include "shared_ptr.inl"

#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:
