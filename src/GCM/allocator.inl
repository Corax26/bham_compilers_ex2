/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Allocator template methods implementation (must not be directly
 * included)
 *
 ******************************************************************************
 */

#ifndef GCM_ALLOCATOR_INL_
#define GCM_ALLOCATOR_INL_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <type_traits>
#include <cstddef>
#include <cstring>
#include <cassert>
#include <algorithm>

/* User */
#include "allocator.h"

/* Forward declarations */

/* Using clauses */


namespace GCM
{
/******************************************************************************
 *                               Public methods                               *
 ******************************************************************************/

template <typename T, typename... Args>
SharedPtr<T> Allocator::New(Args &&...args)
{
    // Type constraints
    static_assert(std::is_destructible<T>(),
                  "Only destructible types can be allocated");
    static_assert(std::is_nothrow_move_constructible<T>(),
                  "Only nothrow-move-constructible types can be allocated");

    // Get a new ref slot
    MemRefInfo &ref = getRefSlot();

    try
    {
        // Allocate memory for the object
        ref.data = allocate(sizeof(T), &ref);
    }
    catch (std::bad_alloc &)
    {
        ref.ref_count.store(GCM::MemRefInfo::INVAL_REF_COUNT,
                            std::memory_order_relaxed);
        throw;
    }

    try
    {
        // Construct the object using a placement new (no dynamic allocation
        // happens, the object is constructed in place at ref.data)
        new(ref.data) T(std::forward<Args>(args)...);
    }
    catch (...)
    {
        // The constructor threw an exception
        // To simplify the allocator's implementation, the memory is not
        // immediately deallocated, instead the reference is marked "no type"
        // and its ref counter remains 0, so that the memory will be reclaimed
        // during next GC
        ref.data_type = MemRefInfo::NO_DATA_TYPE;
        uncollected_garbage_ = true;
        throw;
    }

    // Add type info and update ref count
    ref.data_type = typeid(T);
    ref.ref_count.store(1, std::memory_order_relaxed);


    type_info_index_lock_.lock();
    if (type_info_index_.count(ref.data_type) == 0)
    {
        // Add runtime type info
        TypeInfo &info = type_info_index_[ref.data_type];
        // Add destroy operation if not trivially destructible
        if (!std::is_trivially_destructible<T>())
        {
            info.destroy = [](void *obj) { reinterpret_cast<T*>(obj)->~T(); };
        }
        // Add move operation (useless if it is trivially move constructible,
        // but std::is_trivially_move_constructible is still not implemented
        // as of gcc 4.9)
        info.move = [](void *dest, void *obj)
        {
            new(dest) T(std::move(*reinterpret_cast<T*>(obj)));
            // Calling the destructor is still needed: if T only implements the
            // copy constructor, obj is copied instead of being moved
            reinterpret_cast<T*>(obj)->~T();
        };
    }
    type_info_index_lock_.unlock();

    // The object was properly allocated, build a shared pointer to it
    return SharedPtr<T>{&ref};
}

/******************************************************************************
 *                             Protected methods                              *
 ******************************************************************************/

template <typename B1, typename B2>
void Allocator::moveBlock(B1 *dest, B2 *src)
{
    // Make sure layouts match
    static_assert(offsetof(B1, ref) == offsetof(B2, ref) &&
                  offsetof(B1, data_size) == offsetof(B2, data_size),
                  "Bad block layout");

    MemRefInfo &ref = *src->ref;
    auto data_size = src->data_size;
    assert(ref.data == src->data);

    // Move header
    memmove(dest, src, std::min(sizeof(B2), sizeof(B1)));

    // Move object
    assert(ref.data_type != MemRefInfo::NO_DATA_TYPE);
    assert(type_info_index_.count(ref.data_type));
    auto &move = type_info_index_[ref.data_type].move;
    if (move) move(dest->data, src->data);
    else memcpy(dest->data, src->data, data_size);

    // Update ref
    ref.data = dest->data;
}

/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:


