/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Weak pointer implementation (must not be directly included)
 *
 ******************************************************************************
 */

#ifndef GCM_WEAK_PTR_INL_
#define GCM_WEAK_PTR_INL_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cassert>

/* User */
#include "weak_ptr.h"

/* Forward declarations */

/* Using clauses */


namespace GCM
{
/******************************************************************************
 *                               Public methods                               *
 ******************************************************************************/

template <typename T>
unsigned int WeakPtr<T>::RefCount() const
{
    return ref_ ? ref_->ref_count.load(std::memory_order_relaxed) : 0;
}

template <typename T>
WeakPtr<T>::WeakPtr(const SharedPtr<T> &shared_ptr) noexcept
    : ref_(shared_ptr.ref_)
{}

template <typename T>
T& WeakPtr<T>::operator*() const
{
    return *reinterpret_cast<T*>(ref_->data);
}

template <typename T>
T* WeakPtr<T>::operator->() const
{
    return reinterpret_cast<T*>(ref_->data);
}

template <typename T>
WeakPtr<T>::operator bool() const noexcept
{
    return ref_ != nullptr;
}

template <typename T>
WeakPtr<T>& WeakPtr<T>::operator=(const SharedPtr<T> &shared_ptr)
{
    ref_ = shared_ptr.ref_;
    return *this;
}

/******************************************************************************
 *                             Protected methods                              *
 ******************************************************************************/


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:


