/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   A weak pointer implementation relying on an Allocator object.
 *
 ******************************************************************************
 */

#ifndef GCM_WEAK_PTR_H_
#define GCM_WEAK_PTR_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <utility>

/* User */
#include "mem_ref.h"
#include "shared_ptr.h"

/* Forward declarations */


namespace GCM
{

/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

/**
 * \brief A weak pointer to an object managed by an Allocator.
 *
 * A weak pointer can only be constructed from a shared pointer. Unlike the
 * latter, it doesn't own the reference and doesn't increase the reference
 * counter. For this reason, you can use it to break a dependency cycle,
 * however you have to be careful not to use a weak pointer after all the
 * shared pointers owning its reference have been destroyed.
 *
 * If the reference the weak pointer has been constructed with is not valid
 * any more, any operation other than destroying, copying or moving the weak
 * pointer yield undefined results.
 */
template <typename T>
class WeakPtr
{
public:
    /* Public constants */

    /* Public types */

    /* Public static methods */

    /* Public methods */
    /**
     * \brief Returns the current number of references on the owned object
     * (not including weak pointers).
     */
    unsigned int RefCount() const;

    /* Getters, setters */

    /* Constructors, destructor */
    /**
     * \brief Construct an empty weak pointer, with no reference.
     *
     * Such an object shall only be assigned another weak/shared pointer
     * or destroyed.
     */
	WeakPtr() noexcept
        : ref_(nullptr)
    {}

    /**
     * \brief Initialise the weak pointer with \a shared_ptr's object
     * reference.
     */
    WeakPtr(const SharedPtr<T> &shared_ptr) noexcept;

    WeakPtr(const WeakPtr<T> &other) = default;
    WeakPtr(WeakPtr<T> &&other) = default;

	~WeakPtr() = default;

    /* Operators */
    T& operator*() const;
    T* operator->() const;

    explicit operator bool() const noexcept;

    WeakPtr<T>& operator=(const WeakPtr<T> &shared_ptr) = default;
    WeakPtr<T>& operator=(WeakPtr<T> &&shared_ptr) = default;

    WeakPtr<T>& operator=(const SharedPtr<T> &shared_ptr);

protected:
    /* Protected constants */

    /* Protected types */

    /* Protected methods */

    /* Protected attributes */
	MemRefInfo *ref_;

    friend void swap(WeakPtr<T> &lhs, WeakPtr<T> &rhs)
    {
        std::swap(lhs.ref_, rhs.ref_);
    }
};


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace

// WeakPtr implementation
#include "weak_ptr.inl"

#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:

