/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 *
 ******************************************************************************
 */


/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */

/* User */
#include "mem_ref.h"

/* Forward declarations */

/* Using clauses */

/******************************************************************************
 *                            Private identifiers                             *
 ******************************************************************************/
namespace
{
/* Types */

/* Functions */

} // Unnamed namespace

namespace GCM
{
/******************************************************************************
 *                               Public methods                               *
 ******************************************************************************/


/******************************************************************************
 *                             Protected methods                              *
 ******************************************************************************/


/******************************************************************************
 *                        Static member initialisation                        *
 ******************************************************************************/

const std::type_index   MemRefInfo::NO_DATA_TYPE = typeid(nullptr);
const unsigned int      MemRefInfo::INVAL_REF_COUNT =
    std::numeric_limits<unsigned int>::max();

/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace

// vim: expandtab tabstop=4 shiftwidth=4:

