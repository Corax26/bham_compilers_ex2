/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Some useful constants.
 *
 ******************************************************************************
 */

#ifndef GCM_CONFIG_H_
#define GCM_CONFIG_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cstddef>

/* User */

/* Forward declarations */


namespace GCM
{

/**
 * \brief The minimum alignment value any allocated block.
 *
 * Using this value makes sure that the alignment will be sufficient for any
 * type.
 */
constexpr size_t DATA_ALIGN = alignof(std::max_align_t);

/**
 * \brief Default value used by allocators to deduce the number of references
 * needed for a given memory pool size.
 */
constexpr size_t DEFAULT_REF_SIZE_RATIO = 32;


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:

