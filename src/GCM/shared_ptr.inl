/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Shared pointer implementation (must not be directly included)
 *
 ******************************************************************************
 */

#ifndef GCM_SHARED_PTR_INL_
#define GCM_SHARED_PTR_INL_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cassert>

/* User */
#include "shared_ptr.h"

/* Forward declarations */

/* Using clauses */


namespace GCM
{
/******************************************************************************
 *                               Public methods                               *
 ******************************************************************************/

template <typename T>
unsigned int SharedPtr<T>::RefCount() const
{
    return ref_ ? ref_->ref_count.load(std::memory_order_relaxed) : 0;
}

template <typename T>
void SharedPtr<T>::Release()
{
    decCounter();
    ref_ = nullptr;
}

template <typename T>
SharedPtr<T>::SharedPtr(const SharedPtr<T> &other) noexcept
    : ref_(other.ref_)
{
    // Increment ref counter (there is no acquire semantics, so a relaxed memory
    // order is sufficient)
    if (ref_) ref_->ref_count.fetch_add(1, std::memory_order_relaxed);
}

template <typename T>
SharedPtr<T>::SharedPtr(SharedPtr<T> &&other) noexcept
    : ref_(other.ref_)
{
    // Reset the stolen pointer to prevent the ref counter from being decremented
    other.ref_ = nullptr;
}

template <typename T>
SharedPtr<T>::~SharedPtr()
{
    decCounter();
}

template <typename T>
T& SharedPtr<T>::operator*() const
{
    return *reinterpret_cast<T*>(ref_->data);
}

template <typename T>
T* SharedPtr<T>::operator->() const
{
    return reinterpret_cast<T*>(ref_->data);
}

template <typename T>
SharedPtr<T>::operator bool() const noexcept
{
    return ref_ != nullptr;
}

template <typename T>
SharedPtr<T>& SharedPtr<T>::operator=(const SharedPtr<T> &other) noexcept
{
    // Give up on current object reference
    decCounter();

    // Acquire a new reference to other's object
    ref_ = other.ref_;
    if (ref_) ref_->ref_count.fetch_add(1, std::memory_order_relaxed);

    return *this;
}

template <typename T>
SharedPtr<T>& SharedPtr<T>::operator=(SharedPtr<T> &&other) noexcept
{
    // If other is this object, no counter must be modified
    if (this == &other) return *this;

    // Give up on current object reference
    decCounter();

    // Acquire other's object reference
    ref_ = other.ref_;
    // Reset the stolen pointer to prevent the ref counter from being decremented
    other.ref_ = nullptr;

    return *this;
}

/******************************************************************************
 *                             Protected methods                              *
 ******************************************************************************/

template <typename T>
void SharedPtr<T>::decCounter()
{
    if (ref_)
    {
        // Decrement ref counter (there is no release semantics, so a relaxed memory
        // order is sufficient)
        auto old_counter = ref_->ref_count.fetch_sub(1, std::memory_order_relaxed);
        assert(old_counter != 0);
        if (old_counter == 1)
        {
            // The counter dropped to 0
            *ref_->uncollected_garbage = true;
        }
    }
}

/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:

