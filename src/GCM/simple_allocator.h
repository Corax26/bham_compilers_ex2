/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Simple allocator with a single memory pool and a non-moving GC.
 *
 ******************************************************************************
 */

#ifndef GCM_SIMPLE_ALLOCATOR_H_
#define GCM_SIMPLE_ALLOCATOR_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cstdint>
#include <mutex>

/* User */
#include "allocator.h"
#include "Util/array.h"

/* Forward declarations */


namespace GCM
{

/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

/**
 * \brief A simple allocator with a single memory pool and a non-moving GC.
 *
 * This allocator uses as single pool, in a way similar to heap allocators like
 * malloc. Blocks are never moved once allocated; the free space is arranged in
 * a free list (list of free blocks holding a pointer to the next one).
 *
 * Several algorithms can be used for allocating the blocks (see
 * \a AllocationAlgorithm).
 *
 * The garbage collector just destroys the unreferenced objects, and
 * coalesces the adjacent free blocks together (note that because of that,
 * free blocks are always surrounded with allocated blocks).
 *
 * The pool is initally seen as a single free block, which is then split
 * when blocks need to be allocated.
 */
class SimpleAllocator : public Allocator
{
public:
    /* Public constants */

    /* Public types */
    /**
     * \brief The possible algorithms to allocate new blocks.
     *
     * Among the free blocks whose size is at least the required size,
     * allocate() chooses:
     * - the first one (starting from the beginning of the pool) for FIRST_FIT
     * - the smallest one for BEST_FIT
     * - the first one starting from the free block following the last allocated
     *   block for NEXT_FIT (aka rotating first fit)
     */
    enum AllocationAlgorithm
    {
        FIRST_FIT,
        BEST_FIT,
        NEXT_FIT
    };

    /* Public static methods */

    /* Public methods */

    /* Getters, setters */

    /* Constructors, destructor */
    /**
     * \brief Constructs a simple allocator.
     *
     * The full constructor is used with a reasonable default value for the
     * reference pool size, namely mem_pool_size/DEFAULT_REF_SIZE_RATIO (see
     * config.h)
     */
    explicit SimpleAllocator(size_t mem_pool_size,
                             AllocationAlgorithm alloc_algo = FIRST_FIT,
                             bool mt_opt = false);

    /**
     * \brief Constructs a simple allocator.
     *
     * \param mem_pool_size the total size of the memory pool (aligned up on
     *                      DATA_ALIGN if necessary)
     * \param ref_pool_size the size of the reference pool
     *                      (see Allocator::mem_ref_pool_)
     * \param alloc_algo    the algorithm used for allocating blocks
     * \param mt_opt        a hint: if true, try to optimise for multithread use
     */
    SimpleAllocator(size_t mem_pool_size,
                    size_t ref_pool_size,
                    AllocationAlgorithm alloc_algo = FIRST_FIT,
                    bool mt_opt = false);

    ~SimpleAllocator()
    { GarbageCollection(true); }

    /* Operators */

protected:
    /* Protected constants */

    /* Protected types */
    // Defined in simple_allocator.cpp for brevity
    struct Block;

    /* Protected methods */
    void *allocate(size_t size, MemRefInfo *ref) override final;
    void doGCSingleThread(bool full) override final;

    /* Protected attributes */
    /**
     * \brief The memory pool where all the memory blocks are allocated.
     */
    Util::Array<uint8_t> mem_pool_;

    /**
     * \brief The algorithm used to allocate new memory blocks.
     */
    const AllocationAlgorithm alloc_algo_;

    /**
     * \brief A pointer to the (previous-to-)first free block that is
     * considered by allocate().
     *
     * Since the free list is a single linked list, we can't get the previous
     * free block, that's why we keep track of the previous-to-first free block
     * (which also allows to access the first block).
     *
     * When using the first fit and best fit algorithm, this is effectively the
     * last free block in the memory pool. However, for the next fit algorithm,
     * it is instead the free block just before the last allocated block.
     *
     * Set to nullptr if there is no free block left.
     */
    Block *first_free_block_prev_;

    std::mutex alloc_mutex_;

#ifndef NDEBUG
    size_t free_space_size_;
#endif
};


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:
