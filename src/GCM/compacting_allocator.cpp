/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 *
 ******************************************************************************
 */


/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cassert>
#include <cstring>

/* User */
#include "compacting_allocator.h"
#include "config.h"
#include "Util/align.h"
#include "Util/log.h"

/* Forward declarations */

/* Using clauses */

/******************************************************************************
 *                            Private identifiers                             *
 ******************************************************************************/
namespace
{
/* Types */
/**
 * \brief The building unit of the memory pool.
 *
 * The memory pool is a sequence of blocks. The size of the blocks is variable:
 * the actual size of \a data is \a data_size.
 *
 * Unlike SimpleAllocator, there is only one kind of block: all blocks are used,
 * there is only one free zone at the end of the pool.
 *
 * The first field of the block is a pointer to the memory reference pointing
 * to this block's data (it is needed both to destroy and move the stored
 * object).
 *
 * This struct is just used as a "template" when manipulating the memory pool,
 * which is defined as a raw array of bytes.
 */
struct Block
{
    GCM::MemRefInfo *ref;
    size_t data_size;
    // Use alignas to make really sure that the data will be properly aligned
    alignas(GCM::DATA_ALIGN) uint8_t data[];

    Block() = delete; // Never directly instanciated
};

/* Functions */

} // Unnamed namespace

namespace GCM
{

/******************************************************************************
 *                               Public methods                               *
 ******************************************************************************/

CompactingAllocator::CompactingAllocator(size_t mem_pool_size,
                                         bool mt_opt)
    : CompactingAllocator(mem_pool_size, mem_pool_size/DEFAULT_REF_SIZE_RATIO,
                          mt_opt)
{}

CompactingAllocator::CompactingAllocator(size_t mem_pool_size,
                                         size_t ref_pool_size,
                                         bool mt_opt)
    : Allocator(ref_pool_size, mt_opt),
      mem_pool_(Util::AlignUpSize(mem_pool_size, DATA_ALIGN)),
      free_zone_(mem_pool_.data)
{}


/******************************************************************************
 *                             Protected methods                              *
 ******************************************************************************/

void *CompactingAllocator::allocate(size_t size, MemRefInfo *ref)
{
    uint8_t * const pool_end = mem_pool_.data + mem_pool_.size;

    /* Align size */
    // We always allocate multiples of the alignment size
    size = Util::AlignUpSize(size, DATA_ALIGN);


    /* Reserve space */
    // Reserving space boils down to updating free_zone_ (if there is enough
    // space)
    // We don't even need a mutex to do that in a thread-safe way, atomics at
    // work! A compare-exchange operation is used to ensure that it works,
    // retrying if it fails (when another thread updates the value between
    // the moment we load the value and the moment we try to update it)
    uint8_t *allocated_block;
    uint8_t *new_free_zone;
    do
    {
        allocated_block = free_zone_.load(std::memory_order_relaxed);
        new_free_zone = allocated_block + sizeof(Block) + size;

        if (new_free_zone > pool_end)
        {
            // Not enough space
            cdebug << "CompactingAllocator: Not enough free space left"
                   << std::endl;
            throw std::bad_alloc{};
        }
    }
    while (!free_zone_.compare_exchange_weak(allocated_block, new_free_zone,
                                             std::memory_order_relaxed));


    /* Update block and return */
    assert(reinterpret_cast<uintptr_t>(allocated_block) % DATA_ALIGN == 0);
    Block *block = reinterpret_cast<Block*>(allocated_block);
    block->ref = ref;
    block->data_size = size;

    cdebug << "CompactingAllocator: Allocated " << size + sizeof(Block)
           << "B block @offset " << allocated_block - mem_pool_.data
           << ", " << pool_end - new_free_zone
           << "B left" << std::endl;

    return block->data;
}


// Minor and major collections are the same
void CompactingAllocator::doGCSingleThread(bool)
{
    /*
     * The algorithm is simple: if a block is freed, all the blocks after it
     * are moved so that no free space is left.
     */
    uint8_t * const pool_start = mem_pool_.data;
    // void* just to avoid warnings
    void * const allocated_pool_end = free_zone_.load(std::memory_order_relaxed);

    // The current "virtual" free zone, where the blocks are moved one after
    // the other if they're not destroyed
    uint8_t *current_free_zone = pool_start;
    Block *curr = reinterpret_cast<Block*>(pool_start);
    Block *prev = nullptr; // Only needed for a corner case
    while (curr < allocated_pool_end)
    {
        // Save it in case curr becomes invalid
        auto curr_data_size = curr->data_size;

        if (curr->ref->ref_count.load(std::memory_order_relaxed) == 0)
        {
            // The reference is dead, destroy the object and invalidate
            // the reference
            destroyObject(*curr->ref);
            curr->ref->ref_count.store(MemRefInfo::INVAL_REF_COUNT,
                                       std::memory_order_relaxed);
            cdebug << "CompactingAllocator: Reclaimed " << curr_data_size + sizeof(Block)
                   << "B block @offset " << reinterpret_cast<uint8_t*>(curr) - pool_start
                   << std::endl;
        }
        else
        {
            // The reference is still alive, keep the object and move it if
            // necessary
            ptrdiff_t diff = reinterpret_cast<uint8_t*>(curr) - current_free_zone;
            if (diff != 0)
            {
                assert(diff > 0);
                if (curr_data_size > static_cast<size_t>(diff))
                {
                    // No luck, the new location of the object would overlap the
                    // old one... Move constructors don't expect that so it
                    // cannot be safe!
                    // There is no good solution, I took the easy/safe one:
                    // just add the additional space to the previous block and
                    // leave the current one where it is
                    assert(prev);
                    prev->data_size += diff;
                    current_free_zone += diff;
                    cdebug << "CompactingAllocator: GC overlapping issue, wasted "
                           << diff << "B" << std::endl;
                }
                else
                {
                    // No overlap, move the block (header and data) and update
                    // the reference
                    moveBlock(reinterpret_cast<Block*>(current_free_zone), curr);
                }
            }

            current_free_zone += sizeof(Block) + curr_data_size;
        }

        prev = curr;
        // curr->data is not dereferenced, this is valid even if curr is not
        // valid anymore
        curr = reinterpret_cast<Block*>(curr->data + curr_data_size);
    }

    // The last block should end exactly at the beginning of the old free zone
    assert(curr == allocated_pool_end);

    // Update free zone
    assert(current_free_zone <= allocated_pool_end);
    free_zone_.store(current_free_zone, std::memory_order_relaxed);

    cdebug << "CompactingAllocator: GC completed, free space: "
           << pool_start + mem_pool_.size - current_free_zone << "B" << std::endl;
}

/******************************************************************************
 *                        Static member initialisation                        *
 ******************************************************************************/


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace

// vim: expandtab tabstop=4 shiftwidth=4:

