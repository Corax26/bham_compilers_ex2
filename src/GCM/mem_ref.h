/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Storage of memory references owned by smart pointers.
 *
 ******************************************************************************
 */

#ifndef GCM_MEM_REF_H_
#define GCM_MEM_REF_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <atomic>
#include <limits>
#include <typeindex>

/* User */

/* Forward declarations */


namespace GCM
{

/******************************************************************************
 *                                   Types                                    *
 ******************************************************************************/

/**
 * \brief Memory reference information shared by all smart pointers referencing
 * the same object.
 *
 * The memory reference is valid iff ref_count != INVAL_REF_COUNT.
 */
struct MemRefInfo
{
    static const unsigned int INVAL_REF_COUNT; /**< \a ref_count value used
                                                 when the reference is not valid
                                                 */
    static const std::type_index NO_DATA_TYPE; /**< \a data_type value used
                                                 when no type is associated to
                                                 the data */

    void *data; /**< Pointer to the actual data; putting it here instead of the
                  smart pointer object allows the garbage collector to modify
                  it (e.g. when moving objects between pools) */
    std::type_index data_type; /**< Identifier of the actual type of \a data,
                                 used by the allocator to get runtime type
                                 information */
    std::atomic<unsigned int> ref_count; /**< Atomic reference count to support
                                           cross-thread object sharing */

    bool *uncollected_garbage; /**< A pointer to the allocator's uncollected
                                 garbage marker, see Allocator::uncollected_garbage_
                                 */

    // Construct an invalid reference, data_type is initialised to a dummy value
    // (std::type_index doesn't have a default constructor)
    explicit MemRefInfo(bool *uncollected_garbage)
        : data(nullptr), data_type(NO_DATA_TYPE), ref_count(INVAL_REF_COUNT),
          uncollected_garbage(uncollected_garbage)
    {}

    // Work around atomic's delete copy constructor...
    MemRefInfo(const MemRefInfo &other)
        : data(other.data), data_type(other.data_type),
          ref_count(other.ref_count.load(std::memory_order_relaxed)),
          uncollected_garbage(other.uncollected_garbage)
    {}
};

/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:

