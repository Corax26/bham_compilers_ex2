/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 *
 ******************************************************************************
 */


/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <thread>
#include <cassert>
#include <cstring>

/* User */
#include "allocator.h"
#include "Util/log.h"

/* Forward declarations */

/* Using clauses */

/******************************************************************************
 *                            Private identifiers                             *
 ******************************************************************************/
namespace
{
/* Types */

/* Functions */

} // Unnamed namespace

namespace GCM
{
/******************************************************************************
 *                               Public methods                               *
 ******************************************************************************/

// See allocator.inl for New()

void Allocator::GarbageCollection(bool full, unsigned int nb_thread)
{
    if (nb_thread == 0) return;

    if (nb_thread == 1)
    {
        // No need for synchronisation, just do a single thread GC
        // If doing a full collection, do a GC cycle as long as references
        // become unused when destroying objects (if there is nothing to
        // collect, nothing is done)
        while (uncollected_garbage_)
        {
            uncollected_garbage_ = false;
            doGCSingleThread(full);
            if (!full) break;
        }
    }
    else
    {
        // Wait for all threads and get a unique ID
        auto id = gc_barrier_.Wait(nb_thread);

        // Same kind of loop as above
        while (uncollected_garbage_)
        {
            // Got to wait for the other threads before modifying the marker...
            gc_barrier_.Wait(nb_thread);
            uncollected_garbage_ = false;

            // Got to wait again in case the GC cycle modified the marker...
            gc_barrier_.Wait(nb_thread);
            doGCMultiThread(full, id, nb_thread);

            // Wait for the GC to complete
            gc_barrier_.Wait(nb_thread);

            if (!full) break;
        }
    }
}

Allocator::~Allocator()
{
#ifndef NDEBUG
    // Check if references are still used
    for (auto &ref : mem_ref_pool_)
    {
        // Shouldn't happen after GC
        assert(ref.ref_count.load(std::memory_order_relaxed)
               == MemRefInfo::INVAL_REF_COUNT);
    }
#endif
}

/******************************************************************************
 *                             Protected methods                              *
 ******************************************************************************/

MemRefInfo &Allocator::getRefSlot()
{
    /*
     * Since all slots have the same size, we don't have to worry about
     * fragmentation; that's why a dumb linear search is enough IMO.
     *
     * If we assume only a single thread uses the allocator, we always start
     * at the beginning (this could probably be optimised by memorising the
     * position of the last returned slot, but I don't think this is a critical
     * path in a real world program).
     *
     * If multithread use is assumed, we try to avoid cache trashing by
     * starting the search at a different location depending on the thread ID.
     * That way we can hopefully put the reference slots of different threads
     * in different cache lines, avoiding false sharing. This strategy assumes
     * that most references are not shared between threads (in that case cache
     * trashing will happen anyway).
     */
    size_t start_idx = 0;
    if (mt_opt_)
    {
        // Use the hash of the thread ID
        start_idx = std::hash<std::thread::id>()(std::this_thread::get_id())
                        % mem_ref_pool_size_;
    }

    auto i = start_idx;
    do
    {
        auto &ref = mem_ref_pool_[i];
        auto wanted = MemRefInfo::INVAL_REF_COUNT;
        // Atomically set the counter to 0 if the slot is free, otherwise do
        // nothing
        // There is no need for a strong memory ordering, the slot itself won't
        // be accessed by another thread
        if (ref.ref_count.compare_exchange_strong(wanted, 0,
                                                  std::memory_order_relaxed))
        {
            // The slot was free, we set the counter to 0, now return it
            return ref;
        }
    }
    while ((i = (i + 1) % mem_ref_pool_size_) != start_idx);

    // Didn't find a free slot
    cdebug << "Allocator: No free slot left" << std::endl;
    throw std::bad_alloc{};
}

void Allocator::destroyObject(MemRefInfo &ref)
{
    if (ref.data_type != MemRefInfo::NO_DATA_TYPE)
    {
        assert(type_info_index_.count(ref.data_type));
        auto &destructor = type_info_index_[ref.data_type].destroy;
        if (destructor) destructor(ref.data);
    }
}

void Allocator::doGCMultiThread(bool full, unsigned int id, unsigned int)
{
    if (id == 0) doGCSingleThread(full);
}

/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace

// vim: expandtab tabstop=4 shiftwidth=4:
