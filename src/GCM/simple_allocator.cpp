/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 *
 ******************************************************************************
 */


/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cassert>

/* User */
#include "simple_allocator.h"
#include "config.h"
#include "Util/align.h"
#include "Util/log.h"

/* Forward declarations */

/* Using clauses */

/******************************************************************************
 *                            Private identifiers                             *
 ******************************************************************************/
namespace
{
/* Types */

/* Functions */

} // Unnamed namespace

namespace GCM
{

/**
 * \brief The building unit of the memory pool.
 *
 * The memory pool is a sequence of blocks. The size of the blocks is variable:
 * the actual size of \a data is \a data_size.
 *
 * There are two types of blocks:
 * - Used blocks, containing data of size \a data_size after the header (in \a data)
 * - Free blocks, with \a data_size bytes available. The free blocks are
 *   arranged in a free list: a pointer to the next free block (\a next_free)
 *   is stored. The list is kept circular (the last block points to the first).
 *
 * The first field of the block is:
 * - If the block is used, a pointer to the memory reference pointing
 *   to this block's data (it is needed to know what destructor to call when
 *   freeing the block).
 * - If the block is free, the pointer to the next free block.
 * For ease of use, a union is used to manipulate this pointer.
 *
 * This struct is just used as a "template" when manipulating the memory pool,
 * which is defined as a raw array of bytes. Technically, this violates strict
 * aliasing and is a case of undefined behaviour, but since we never ever read
 * from or write directly to the pool as an array of bytes I don't think it is
 * an issue in practice.
 */
struct SimpleAllocator::Block
{
    union
    {
        void *first_field;
        MemRefInfo *ref;
        Block *next_free;
    };
    size_t data_size;
    // Use alignas to make really sure that the data will be properly aligned
    alignas(DATA_ALIGN) uint8_t data[];

    Block() = delete; // Never directly instanciated
};

/******************************************************************************
 *                               Public methods                               *
 ******************************************************************************/

SimpleAllocator::SimpleAllocator(size_t mem_pool_size,
                                 AllocationAlgorithm alloc_algo,
                                 bool mt_opt)
    : SimpleAllocator(mem_pool_size, mem_pool_size/DEFAULT_REF_SIZE_RATIO,
                      alloc_algo, mt_opt)
{}

SimpleAllocator::SimpleAllocator(size_t mem_pool_size,
                                 size_t ref_pool_size,
                                 AllocationAlgorithm alloc_algo,
                                 bool mt_opt)
    : Allocator(ref_pool_size, mt_opt),
      mem_pool_(Util::AlignUpSize(mem_pool_size, DATA_ALIGN)),
      alloc_algo_(alloc_algo),
      first_free_block_prev_(reinterpret_cast<Block*>(mem_pool_.data))
#ifndef NDEBUG
      , free_space_size_(mem_pool_.size)
#endif
{
    // Initialise the memory pool as a single free block
    // Note on alignment: std::vector allocates its array using malloc()
    // which must return a block satisfying the largest alignment requirement,
    // so using it as the first free block is correct in terms of alignment
    first_free_block_prev_->next_free = first_free_block_prev_;
    first_free_block_prev_->data_size = mem_pool_.size - sizeof(Block);
}

/******************************************************************************
 *                             Protected methods                              *
 ******************************************************************************/

void *SimpleAllocator::allocate(size_t size, MemRefInfo *ref)
{
    assert(size > 0);

    // We need to lock the whole allocation process
    std::lock_guard<std::mutex> lock(alloc_mutex_);

    /* Align size */
    // We always allocate multiples of the alignment size
    size = Util::AlignUpSize(size, DATA_ALIGN);


    /* Choose a free block */
    Block *chosen_block_prev = nullptr;

    // Abort if no free block left
    if (!first_free_block_prev_)
    {
        cdebug << "SimpleAllocator: No free space left" << std::endl;
        throw std::bad_alloc{};
    }

    Block *prev = first_free_block_prev_;
    Block *curr = first_free_block_prev_->next_free;
    do
    {
        if (curr->data_size >= size)
        {
            // There is enough space in this block
            // Note: optimisation in the case of best fit, if the size
            // exactly matches there is no use looking further
            if (curr->data_size > size && alloc_algo_ == BEST_FIT)
            {
                // Compare to the last chosen block, if any
                if (!chosen_block_prev || chosen_block_prev->next_free->data_size
                                            > curr->data_size)
                {
                    chosen_block_prev = prev;
                }
            }
            else
            {
                // In the first/next fit cases, the first block where
                // the data fits is chosen
                chosen_block_prev = prev;
                break;
            }
        }
    }
    while (prev = curr, curr = curr->next_free, prev != first_free_block_prev_);

    // Abort if there is no big enough block left
    if (!chosen_block_prev)
    {
        cdebug << "SimpleAllocator: Not enough free space left" << std::endl;
        throw std::bad_alloc{};
    }
    Block *chosen_block = chosen_block_prev->next_free;


    /* Update blocks */
    if (chosen_block->data_size >= size + sizeof(Block) + DATA_ALIGN)
    {
        // There is enough space to split the block into the allocated block
        // and a new free block
        // In an actual heap allocator we would just decrease the size of the
        // free block and create a block after it, but here the size of the
        // "heap" is fixed, so if we do that the objects will be allocated
        // descending from the end of the pool, which is a bit disturbing for
        // debugging, so we swap (the free block becomes used, and a free block
        // is created after it)

        // Create free block
        assert(reinterpret_cast<uintptr_t>(chosen_block->data + size)
               % DATA_ALIGN == 0);
        Block *new_free = reinterpret_cast<Block*>(chosen_block->data + size);
        new_free->data_size = chosen_block->data_size - size - sizeof(Block);

        // Adjust block size
        chosen_block->data_size = size;
#ifndef NDEBUG
        free_space_size_ -= size + sizeof(Block);
        cdebug << "SimpleAllocator: Allocated " << size + sizeof(Block)
               << "B block @offset " << reinterpret_cast<uint8_t*>(chosen_block) - mem_pool_.data
               << ", " << free_space_size_ << "B left" << std::endl;
#endif

        // Relink free list
        if (chosen_block == chosen_block_prev)
        {
            // Only one free block left
            assert(chosen_block->next_free = chosen_block);
            new_free->next_free = new_free;
        }
        else
        {
            chosen_block_prev->next_free = new_free;
            new_free->next_free = chosen_block->next_free;
        }

        // Update first_free_block_prev_ if necessary
        if (chosen_block == first_free_block_prev_ ||
                chosen_block == chosen_block_prev)
            first_free_block_prev_ = new_free;
        else if (alloc_algo_ == NEXT_FIT)
            first_free_block_prev_ = chosen_block_prev;
    }
    else
    {
        // Not enough space remaining to split the block, use the whole block
#ifndef NDEBUG
        free_space_size_ -= chosen_block->data_size + sizeof(Block);
        cdebug << "SimpleAllocator: Allocated " << size + sizeof(Block)
               << "B block @offset " << reinterpret_cast<uint8_t*>(chosen_block) - mem_pool_.data
               << " (using whole "
               << chosen_block->data_size + sizeof(Block) << "B block), "
               << free_space_size_ << "B left" << std::endl;
#endif

        // Relink free list and update first_free_block_prev_ if necessary
        if (chosen_block == chosen_block_prev)
        {
            // This was the last free block
            assert(chosen_block->next_free = chosen_block);
            first_free_block_prev_ = nullptr;
        }
        else
        {
            chosen_block_prev->next_free = chosen_block->next_free;
            if (chosen_block == first_free_block_prev_
                    || alloc_algo_ == NEXT_FIT)
                first_free_block_prev_ = chosen_block_prev;
        }
    }

    // Add the reference to the allocated block and return it
    chosen_block->ref = ref;
    return chosen_block->data;
}


// Minor and major collections are the same
void SimpleAllocator::doGCSingleThread(bool)
{
    /*
     * Instead of doing a linear search in the reference pool (which
     * is always large: its size is fixed), we directly inspect the blocks in
     * the memory pool. This is clearly faster if only a few objects are allocated,
     * and it should never buch really much slower (except maybe pathological
     * cases where a bunch of small objects are scattered across the pool).
     *
     * All the blocks are modified in a single pass, coalescing consecutive
     * freed blocks.
     *
     * There is no field in the block header indicating whether it is free
     * or not (with 64b pointers, adding a field would double its size of the
     * header due to alignment). However we need this piece of information, so
     * here is the trick: if it is a free block, we know that the first field
     * is a pointer to somewhere in the pool, otherwise it cannot be.
     */
    uint8_t * const pool_start = mem_pool_.data;
    // void* just to avoid warnings
    void * const pool_end = pool_start + mem_pool_.size;

    // First free block in the pool (including freed blocks), we need it
    // to keep the free list circular
    Block *first_free_block = nullptr;
    // First free block in a group of free blocks to be coalesced
    Block *first_coalescing_free_block = nullptr;
    // First used block after a group of free blocks to be coalesced
    Block *end_coalescing_group = nullptr;
    Block *curr = reinterpret_cast<Block*>(pool_start);
    while (curr < pool_end)
    {
        bool curr_is_free = false;
        // See above for explanation
        if (curr->first_field >= pool_start && curr->first_field < pool_end)
        {
            // This is a free block
            curr_is_free = true;
        }
        else
        {
            // This is a used block
            if (curr->ref->ref_count.load(std::memory_order_relaxed) == 0)
            {
                // The reference is dead, destroy the object and invalidate
                // the reference
                destroyObject(*curr->ref);
                curr->ref->ref_count.store(MemRefInfo::INVAL_REF_COUNT,
                                           std::memory_order_relaxed);
                curr_is_free = true;
#ifndef NDEBUG
                free_space_size_ += curr->data_size + sizeof(Block);
                cdebug << "SimpleAllocator: Reclaimed " << curr->data_size + sizeof(Block)
                       << "B block @offset " << reinterpret_cast<uint8_t*>(curr) - pool_start
                       << std::endl;
#endif
            }
        }

        if (curr_is_free)
        {
            if (!first_free_block) first_free_block = curr;
            if (!first_coalescing_free_block) first_coalescing_free_block = curr;

            if (end_coalescing_group)
            {
                assert(first_coalescing_free_block);
                // Merge the previous group of free blocks, the next free block
                // is the current one
                first_coalescing_free_block->next_free = curr;
                first_coalescing_free_block->data_size =
                    reinterpret_cast<uint8_t*>(end_coalescing_group) -
                    first_coalescing_free_block->data;

                first_coalescing_free_block = curr;
                end_coalescing_group = nullptr;
            }
        }
        else
        {
            // This block is still in use, if it is preceded by a coalescing group
            // it ends it
            if (first_coalescing_free_block && !end_coalescing_group)
                end_coalescing_group = curr;
        }

        curr = reinterpret_cast<Block*>(curr->data + curr->data_size);
    }

    // The last block should end exactly at the end of the pool
    assert(curr == pool_end);

    // Unless no free block is left, we have to coalesce the last group of free
    // blocks and loop the free list
    if (first_coalescing_free_block)
    {
        assert(first_free_block);
        first_coalescing_free_block->next_free = first_free_block;
        first_coalescing_free_block->data_size =
            reinterpret_cast<uint8_t*>(end_coalescing_group ? end_coalescing_group
                                                            : curr) -
            first_coalescing_free_block->data;

        // This block is now the last free block
        first_free_block_prev_ = first_coalescing_free_block;
    }

#ifndef NDEBUG
    cdebug << "SimpleAllocator: GC completed, free space: "
           << free_space_size_ << "B" << std::endl;
#endif
}

/******************************************************************************
 *                        Static member initialisation                        *
 ******************************************************************************/


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace

// vim: expandtab tabstop=4 shiftwidth=4:
