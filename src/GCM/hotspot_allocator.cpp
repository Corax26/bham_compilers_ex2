/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 *
 ******************************************************************************
 */


/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cassert>
#include <cstring>

/* User */
#include "hotspot_allocator.h"
#include "config.h"
#include "Util/align.h"
#include "Util/log.h"

/* Forward declarations */

/* Using clauses */

/******************************************************************************
 *                            Private identifiers                             *
 ******************************************************************************/
namespace
{
/* Types */
/**@{*/
/**
 * \brief The building units of the memory spaces.
 *
 * The memory spaces are a sequence of blocks. The size of the blocks is variable:
 * the actual size of \a data is \a data_size.
 *
 * There are two kind of blocks:
 * - the "normal" one for the eden space and the tenured generation
 * - the "aged" one for the survivor spaces, because we need to store the age of
 *   the blocks to know when to move them to the tenured generation
 *
 * The first field of the block is a pointer to the memory reference pointing
 * to this block's data (it is needed both to destroy and move the stored
 * object).
 *
 * This struct is just used as a "template" when manipulating the memory pool,
 * which is defined as a raw array of bytes.
 */
struct Block
{
    GCM::MemRefInfo *ref;
    size_t data_size;
    // Use alignas to make really sure that the data will be properly aligned
    alignas(GCM::DATA_ALIGN) uint8_t data[];

    Block() = delete; // Never directly instanciated
};

struct AgedBlock
{
    GCM::MemRefInfo *ref;
    size_t data_size;
    size_t age;
    // Use alignas to make really sure that the data will be properly aligned
    alignas(GCM::DATA_ALIGN) uint8_t data[];

    AgedBlock() = delete; // Never directly instanciated
};
/**@}*/

/* Functions */

} // Unnamed namespace

namespace GCM
{
/******************************************************************************
 *                               Public methods                               *
 ******************************************************************************/

HotspotAllocator::HotspotAllocator(size_t total_mem_pool_size,
                                   size_t new_ratio,
                                   size_t survivor_ratio,
                                   size_t age_threshold,
                                   size_t major_gc_threshold,
                                   bool mt_opt)
    : HotspotAllocator(total_mem_pool_size, new_ratio,
                       survivor_ratio, age_threshold,
                       major_gc_threshold,
                       total_mem_pool_size/DEFAULT_REF_SIZE_RATIO,
                       mt_opt)
{}

HotspotAllocator::HotspotAllocator(size_t total_size,
                                   size_t new_r,
                                   size_t survivor_r,
                                   size_t age_threshold,
                                   size_t major_gc_threshold,
                                   size_t ref_pool_size,
                                   bool mt_opt)
    : Allocator(ref_pool_size, mt_opt),
      age_threshold_(age_threshold), major_gc_threshold_(major_gc_threshold),
      need_major_gc_(false),
      eden_space_(Util::AlignUpSize(total_size * survivor_r /
                                    ((1 + new_r) * (2 + survivor_r)), DATA_ALIGN)),
      eden_free_zone_(eden_space_.data),
      active_survivor_space_(Util::AlignUpSize(total_size /
                                               ((1 + new_r) * (2 + survivor_r)),
                                               DATA_ALIGN)),
      passive_survivor_space_(active_survivor_space_.size),
      survivor_free_zone_(active_survivor_space_.data),
      tenured_generation_(Util::AlignUpSize(total_size * new_r / (1 + new_r),
                                            DATA_ALIGN)),
      tenured_free_zone_(tenured_generation_.data)
{
    cdebug << "HotspotAllocator created, eden size = " << eden_space_.size
           << "; survivor size = " << active_survivor_space_.size
           << "; tenured gen size = " << tenured_generation_.size
           << std::endl;
}


/******************************************************************************
 *                             Protected methods                              *
 ******************************************************************************/

void *HotspotAllocator::allocate(size_t size, MemRefInfo *ref)
{
    /*
     * This is exactly the same algorithm as CompactingAllocator::allocate()
     */

    uint8_t * const eden_end = eden_space_.data + eden_space_.size;

    /* Align size */
    // We always allocate multiples of the alignment size
    size = Util::AlignUpSize(size, DATA_ALIGN);


    /* Reserve space */
    uint8_t *allocated_block;
    uint8_t *new_free_zone;
    do
    {
        allocated_block = eden_free_zone_.load(std::memory_order_relaxed);
        new_free_zone = allocated_block + sizeof(Block) + size;

        if (new_free_zone > eden_end)
        {
            // Not enough space
            cdebug << "HotspotAllocator: Not enough free space left in eden space"
                   << std::endl;
            throw std::bad_alloc{};
        }
        else
        {
            // Maximum ratio of growth of a block when going from eden to
            // survivor space (AgedBlock has an additional field), the worst
            // case being when data_size is minimal (DATA_ALIGN)
            constexpr double max_block_growth =
                static_cast<double>((DATA_ALIGN) + sizeof(AgedBlock)) /
                                   (DATA_ALIGN + sizeof(Block));
            auto max_eden_curr_size_in_survivor = static_cast<size_t>(
                (new_free_zone - eden_space_.data) * max_block_growth);
            // No thread-safety issue: the values are only modified during GC
            size_t tenured_gen_free_size = tenured_generation_.data +
                tenured_generation_.size - tenured_free_zone_;
            if (max_eden_curr_size_in_survivor > tenured_gen_free_size)
            {
                // Enough space, but failure may happen during GC because the
                // tenured generation cannot contain the eden space: when
                // a minor GC is done, eden blocks are transferred to survivor
                // space unless it is full, in which case they're transferred
                // to tenured gen.
                // -> To prevent GC failure, prevent allocation
                cdebug << "HotspotAllocator: Not enough free space left in tenured "
                       << "generation, allocation refused to prevent GC failure"
                       << std::endl;
                // Do a major GC next time GarbageCollection() is called
                // Thread-safe because outside of GC, only true is written to it
                need_major_gc_ = true;
                throw std::bad_alloc{};
            }
        }
    }
    while (!eden_free_zone_.compare_exchange_weak(allocated_block, new_free_zone,
                                                  std::memory_order_relaxed));


    /* Update block and return */
    assert(reinterpret_cast<uintptr_t>(allocated_block) % DATA_ALIGN == 0);
    Block *block = reinterpret_cast<Block*>(allocated_block);
    block->ref = ref;
    block->data_size = size;

    cdebug << "HotspotAllocator: Allocated " << size + sizeof(Block)
           << "B block @offset " << allocated_block - eden_space_.data
           << ", " << eden_end - new_free_zone
           << "B left in eden space" << std::endl;

    return block->data;
}


void HotspotAllocator::doGCSingleThread(bool full)
{
    // Issue a minor collection
    minorCollection();

    size_t tenured_gen_free_ratio =
        (tenured_generation_.data + tenured_generation_.size - tenured_free_zone_)
        * 100 / tenured_generation_.size;
    if (full || need_major_gc_ || tenured_gen_free_ratio <= major_gc_threshold_)
    {
        // Also issue a major collection
        majorCollection();
        need_major_gc_ = false;
    }
}

void HotspotAllocator::minorCollection()
{
    /* Swap survivor spaces */
    swap(active_survivor_space_, passive_survivor_space_);
    void *passive_survivor_end = survivor_free_zone_;
    survivor_free_zone_ = active_survivor_space_.data;


    bool survivor_full = false;
    /* Move the eden space to the active survivor space */
    {
        void * const eden_end = eden_free_zone_.load(std::memory_order_relaxed);
        auto curr = reinterpret_cast<Block*>(eden_space_.data);
        while (curr < eden_end)
        {
            if (curr->ref->ref_count.load(std::memory_order_relaxed) == 0)
            {
                // The reference is dead, destroy the object and invalidate
                // the reference
                destroyObject(*curr->ref);
                curr->ref->ref_count.store(MemRefInfo::INVAL_REF_COUNT,
                                           std::memory_order_relaxed);
                cdebug << "HotspotAllocator: Reclaimed " << curr->data_size + sizeof(Block)
                       << "B block in eden space @offset "
                       << reinterpret_cast<uint8_t*>(curr) - eden_space_.data
                       << std::endl;
            }
            else
            {
                // The reference is still alive
                if (!survivor_full &&
                    survivor_free_zone_ + sizeof(AgedBlock) + curr->data_size <=
                    active_survivor_space_.data + active_survivor_space_.size)
                {
                    // Enough space in active survivor space, move the block to
                    // the new active survivor space (and update the reference)
                    auto new_block = reinterpret_cast<AgedBlock*>(survivor_free_zone_);
                    moveBlock(new_block, curr);
                    // Set age
                    new_block->age = 1;

                    cdebug << "HotspotAllocator: Moved " << curr->data_size + sizeof(Block)
                        << "B block from eden space @offset "
                        << reinterpret_cast<uint8_t*>(curr) - eden_space_.data
                        << " to active survivor space @offset "
                        << survivor_free_zone_ - active_survivor_space_.data
                        << std::endl;

                    survivor_free_zone_ += sizeof(AgedBlock) + curr->data_size;
                }
                else
                {
                    // Not enough space in active survivor space, move to
                    // tenured gen
                    // allocate() refuses to allocate a block if the tenured
                    // gen may overflow, so we must have enough space
                    assert(tenured_free_zone_ + sizeof(Block) + curr->data_size <=
                           tenured_generation_.data + tenured_generation_.size);
                    if (!survivor_full)
                    {
                        survivor_full = true;
                        cdebug << "HotspotAllocator: survivor space full, "
                                  "transferring to tenured generation" << std::endl;
                    }

                    auto new_block = reinterpret_cast<Block*>(tenured_free_zone_);
                    moveBlock(new_block, curr);

                    cdebug << "HotspotAllocator: Moved " << curr->data_size + sizeof(Block)
                        << "B block from eden space @offset "
                        << reinterpret_cast<uint8_t*>(curr) - eden_space_.data
                        << " to tenured generation @offset "
                        << tenured_free_zone_ - tenured_generation_.data
                        << std::endl;

                    tenured_free_zone_ += sizeof(Block) + curr->data_size;
                }

            }

            curr = reinterpret_cast<Block*>(curr->data + curr->data_size);
        }

        assert(curr == eden_end);

        // Update free zone
        eden_free_zone_.store(eden_space_.data, std::memory_order_relaxed);
    }

    /* Move the passive survivor space to the active survivor space */
    {
        auto curr = reinterpret_cast<AgedBlock*>(passive_survivor_space_.data);
        while (curr < passive_survivor_end)
        {
            if (curr->ref->ref_count.load(std::memory_order_relaxed) == 0)
            {
                // The reference is dead, destroy the object and invalidate
                // the reference
                destroyObject(*curr->ref);
                curr->ref->ref_count.store(MemRefInfo::INVAL_REF_COUNT,
                                           std::memory_order_relaxed);
                cdebug << "HotspotAllocator: Reclaimed " << curr->data_size + sizeof(AgedBlock)
                       << "B block in survivor space @offset "
                       << reinterpret_cast<uint8_t*>(curr) - passive_survivor_space_.data
                       << std::endl;
            }
            else
            {
                // The reference is still alive
                if (!survivor_full &&
                    survivor_free_zone_ + sizeof(AgedBlock) + curr->data_size >
                        active_survivor_space_.data + active_survivor_space_.size)
                {
                    survivor_full = true;
                    cdebug << "HotspotAllocator: survivor space full, "
                              "transferring to tenured generation" << std::endl;
                }

                if (curr->age >= age_threshold_ || survivor_full)
                {
                    // The object is old enough to be tenured, or there is no
                    // space left in the active survivor space
                    // See above for the reason why it cannot overflow
                    assert(tenured_free_zone_ + sizeof(Block) + curr->data_size <=
                           tenured_generation_.data + tenured_generation_.size);

                    // Move the block to the tenured generation
                    auto new_block = reinterpret_cast<Block*>(tenured_free_zone_);
                    moveBlock(new_block, curr);

                    cdebug << "HotspotAllocator: Moved " << curr->data_size + sizeof(AgedBlock)
                        << "B block from passive survivor space @offset "
                        << reinterpret_cast<uint8_t*>(curr) - passive_survivor_space_.data
                        << " to tenured generation @offset "
                        << tenured_free_zone_ - tenured_generation_.data
                        << " (age was " << curr->age << ")"
                        << std::endl;

                    tenured_free_zone_ += sizeof(Block) + curr->data_size;
                }
                else
                {
                    // Keep the object in survivor space
                    // Move the block to the new active survivor space (and update the
                    // reference)
                    auto new_block = reinterpret_cast<AgedBlock*>(survivor_free_zone_);
                    moveBlock(new_block, curr);
                    // Update age
                    ++new_block->age;

                    cdebug << "HotspotAllocator: Moved " << curr->data_size + sizeof(AgedBlock)
                        << "B block from passive survivor space @offset "
                        << reinterpret_cast<uint8_t*>(curr) - passive_survivor_space_.data
                        << " to active survivor space @offset "
                        << survivor_free_zone_ - active_survivor_space_.data
                        << " (new age is " << new_block->age << ")"
                        << std::endl;

                    survivor_free_zone_ += sizeof(AgedBlock) + curr->data_size;
                }
            }

            curr = reinterpret_cast<AgedBlock*>(curr->data + curr->data_size);
        }

        assert(curr == passive_survivor_end);

        // The free zone is already up-to-date
    }

    cdebug << "HotspotAllocator: minor GC completed, free space in survivor space: "
           << active_survivor_space_.data + active_survivor_space_.size - survivor_free_zone_
           << "B; in tenured generation: "
           << tenured_generation_.data + tenured_generation_.size - tenured_free_zone_
           << "B" << std::endl;
}

void HotspotAllocator::majorCollection()
{
    /*
     * See CompactingAllocator::doGCSingleThread(), this is exactly the same
     * algorithm...
     * TODO: refactor with generic algorithms
     */

    uint8_t * const pool_start = tenured_generation_.data;
    // void* just to avoid warnings
    void * const allocated_pool_end = tenured_free_zone_;

    // The current "virtual" free zone, where the blocks are moved one after
    // the other if they're not destroyed
    uint8_t *current_free_zone = pool_start;
    Block *curr = reinterpret_cast<Block*>(pool_start);
    Block *prev = nullptr; // Only needed for a corner case
    while (curr < allocated_pool_end)
    {
        // Save it in case curr becomes invalid
        auto curr_data_size = curr->data_size;

        if (curr->ref->ref_count.load(std::memory_order_relaxed) == 0)
        {
            // The reference is dead, destroy the object and invalidate
            // the reference
            destroyObject(*curr->ref);
            curr->ref->ref_count.store(MemRefInfo::INVAL_REF_COUNT,
                                       std::memory_order_relaxed);
            cdebug << "HotspotAllocator: Reclaimed " << curr_data_size + sizeof(Block)
                   << "B block in tenured generation @offset "
                   << reinterpret_cast<uint8_t*>(curr) - pool_start
                   << std::endl;
        }
        else
        {
            // The reference is still alive, keep the object and move it if
            // necessary
            ptrdiff_t diff = reinterpret_cast<uint8_t*>(curr) - current_free_zone;
            if (diff != 0)
            {
                assert(diff > 0);
                if (curr_data_size > static_cast<size_t>(diff))
                {
                    // No luck, the new location of the object would overlap the
                    // old one... Move constructors don't expect that so it
                    // cannot be safe!
                    // There is no good solution, I took the easy/safe one:
                    // just add the additional space to the previous block and
                    // leave the current one where it is
                    assert(prev);
                    prev->data_size += diff;
                    current_free_zone += diff;
                    cdebug << "HotspotAllocator: GC overlapping issue, wasted "
                           << diff << "B in tenured generation" << std::endl;
                }
                else
                {
                    // No overlap, move the block (header and data) and update
                    // the reference
                    moveBlock(reinterpret_cast<Block*>(current_free_zone), curr);
                }
            }

            current_free_zone += sizeof(Block) + curr_data_size;
        }

        prev = curr;
        // curr->data is not dereferenced, this is valid even if curr is not
        // valid anymore
        curr = reinterpret_cast<Block*>(curr->data + curr_data_size);
    }

    // The last block should end exactly at the beginning of the old free zone
    assert(curr == allocated_pool_end);

    // Update free zone
    assert(current_free_zone <= allocated_pool_end);
    tenured_free_zone_ = current_free_zone;

    cdebug << "HotspotAllocator: major GC completed, free space in tenured generation: "
           << pool_start + tenured_generation_.size - current_free_zone
           << "B" << std::endl;
}

/******************************************************************************
 *                        Static member initialisation                        *
 ******************************************************************************/


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace

// vim: expandtab tabstop=4 shiftwidth=4:
