/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Allocator based on the HotSpot JVM's GCs (multiple memory pools and
 * moving GC).
 *
 ******************************************************************************
 */

#ifndef GCM_HOTSPOT_ALLOCATOR_H_
#define GCM_HOTSPOT_ALLOCATOR_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cstdint>
#include <atomic>
#include <exception>

/* User */
#include "allocator.h"
#include "Util/array.h"

/* Forward declarations */


namespace GCM
{

/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

/**
 * \brief An allocator based on the HotSpot Java VM's generational collectors.
 *
 * This allocator uses a generational collector, for this reason it uses 4
 * pools:
 * - one eden space where the new objects are allocated
 * - two survivor spaces, where objects surviving up to \a age_threshold_
 *   collections are stored
 * - one tenured generation space, where objects having survived at least
 *   \a age_threshold_ collections are kept
 * The eden and survivor spaces form the young generation.
 *
 * allocate() only uses the eden space to allocate new blocks, and (like the
 * other allocators) doesn't run the GC if there is not enough space, for
 * thread-safety reasons. Because of that, the GC must be manually run often
 * enough to empty the eden space. Additionally, allocate() will fail
 * if there is not enough free space in the tenured generation (otherwise
 * the GC may fail); if this happens a major collection will be run
 * next time GarbageCollection() is called (see allocate() for details).
 *
 * doGCSingleThread() implements HotSpot's serial collector. A minor collection
 * is always done, clearing the eden space and swapping the survivor spaces.
 * If the amount of free space is below \a major_gc_threshold_ or if a full
 * collection has been requested, a major collection is also done, compacting
 * the tenured generation.
 *
 * It should be reasonably easy to implement the parallel collector by overriding
 * doGCMultiThread() (making all the free spaces pointers atomic should be
 * enough in terms of synchronisation).
 */
class HotspotAllocator : public Allocator
{
public:
    /* Public constants */
    static constexpr size_t DEFAULT_NEW_RATIO = 3;
    static constexpr size_t DEFAULT_SURVIVOR_RATIO = 2;
    static constexpr size_t DEFAULT_AGE_THRESHOLD = 6;
    static constexpr size_t DEFAULT_MAJOR_GC_THRESHOLD = 70;

    /* Public types */

    /* Public static methods */

    /* Public methods */

    /* Getters, setters */

    /* Constructors, destructor */
    /**
     * \brief Constructs a hotspot allocator.
     *
     * The full constructor is used with a reasonable default value for the
     * reference pool size, namely total_mem_pool_size/DEFAULT_REF_SIZE_RATIO
     * (see config.h).
     */
    explicit HotspotAllocator(size_t total_mem_pool_size,
                              size_t new_ratio = DEFAULT_NEW_RATIO,
                              size_t survivor_ratio = DEFAULT_SURVIVOR_RATIO,
                              size_t age_threshold = DEFAULT_AGE_THRESHOLD,
                              size_t major_gc_threshold = DEFAULT_MAJOR_GC_THRESHOLD,
                              bool mt_opt = false);

    /**
     * \brief Construct a hotspot allocator.
     *
     * The arguments \a new_ratio and \a survivor_ratio match the JVM's
     * parameters NewRatio and SurvivorRatio.
     *
     * \param total_mem_pool_size   the total size of the memory pools
     *                              (may be actually higher after rounding up
     *                              divisions and aligning sizes)
     * \param new_ratio             the ratio between the young and tenured
     *                              generation (tenured_gen_size / young_gen_size)
     * \param survivor_ratio        the ratio between each survivor space and
     *                              and the eden space (eden_size / survivor_size)
     * \param age_threshold         the threshold for the age of an object
     *                              to be moved from survivor space to tenured
     *                              generation (must be > 1)
     * \param major_gc_threshold    the threshold of free space in the tenured
     *                              generation above which a major GC is
     *                              triggered (in %, must be between 0 and 100)
     * \param ref_pool_size         the size of the reference pool
     *                              (see Allocator::mem_ref_pool_)
     * \param mt_opt                a hint: if true, try to optimise for
     *                              multithread use
     */
    HotspotAllocator(size_t total_mem_pool_size,
                     size_t new_ratio, size_t survivor_ratio,
                     size_t age_threshold, size_t major_gc_threshold,
                     size_t ref_pool_size,
                     bool mt_opt = false);

    ~HotspotAllocator()
    { GarbageCollection(true); }

    /* Operators */

protected:
    /* Protected constants */

    /* Protected types */

    /* Protected methods */
    void *allocate(size_t size, MemRefInfo *ref) override final;
    void doGCSingleThread(bool full) override final;

    /**
     * \brief Do a minor GC (young generation).
     *
     * Swap the survivor spaces, move everything from the eden space and the
     * old active survivor space to the new active survivor space (or the
     * tenured generation).
     *
     * A major collection may be triggered if there is not enough space in the
     * tenured generation.
     */
    void minorCollection();

    /**
     * \brief Do a major GC (tenured generation).
     *
     * Collect unused objects in the tenured generation and compact it.
     */
    void majorCollection();


    /* Protected attributes */
    /**
     * \brief The age threshold considered when deciding whether an object
     * in survivor space should be tenured.
     *
     * An object is tenured when it survives \a age_threshold_ GCs, unless
     * the new active survivor space is filled during minor GC, in which case
     * the object is immediately tenured.
     */
    const size_t age_threshold_;

    /**
     * \brief The free space threshold of the tenured generation considered
     * to trigger a major GC.
     */
    const size_t major_gc_threshold_;

    /**
     * \brief Flag set to true when a major collection is needed (when allocate()
     * fails because the tenured generation has not enough free space).
     */
    bool need_major_gc_;

    /**
     * \brief The eden space (young generation).
     */
    Util::Array<uint8_t> eden_space_;

    /**
     * \brief A pointer to the free zone in the eden space, after the last
     * block in use.
     *
     * If no space is left, points after the end of the eden space.
     *
     * The pointer is made atomic to enable a thread-safe allocate()
     * implementation.
     */
    std::atomic<uint8_t*> eden_free_zone_;

    /**
     * \brief The active survivor space (young generation).
     */
    Util::SwappableArray<uint8_t> active_survivor_space_;

    /**
     * \brief The passive survivor space, swapped with the active space during
     * GC (young generation).
     */
    Util::SwappableArray<uint8_t> passive_survivor_space_;

    /**
     * \brief A pointer to the free zone of the active survivor space.
     */
    uint8_t *survivor_free_zone_;

    /**
     * \brief The tenured generation.
     */
    Util::Array<uint8_t> tenured_generation_;

    /**
     * \brief A pointer to the free zone of the tenured generation.
     */
    uint8_t *tenured_free_zone_;
};


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:


