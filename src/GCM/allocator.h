/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Base allocator class and helpers.
 *
 ******************************************************************************
 */

#ifndef GCM_ALLOCATOR_H_
#define GCM_ALLOCATOR_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <vector>
#include <unordered_map>
#include <typeindex>
#include <mutex>

/* User */
#include "mem_ref.h"
#include "shared_ptr.h"
#include "Util/barrier.h"

/* Forward declarations */


namespace GCM
{

/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

/**
 * \brief A memory allocator and object manager.
 *
 * This class manages objects that can be created with the New() method. The
 * objects are allocated by the class itself (not through malloc()).
 *
 * The managed objects can only be referred to through smart pointers, so that
 * reference counting can be used to determine if they are still alive.
 *
 * Smart pointers don't destroy the objects themselves when the counter drops to
 * 0, unlike std::shared_ptr. Instead, the dead objects are all collected in a
 * single garbage collection cycle that is triggered when GarbageCollection()
 * is called.
 *
 * As a consequence, the lifetime of the allocator shall extend after the
 * lifetime of any shared pointer holding a reference to its data. A garbage
 * collection is triggered when the allocator is destroyed.
 *
 * The allocator and the smart pointers can be used by multiple threads. In
 * that case, all the threads using the same allocator must call
 * GarbageCollection() to launch a garbage collection (see the method's doc).
 *
 * This class is abstract, common methods are implemented but the allocation
 * and garbage collection strategies must be implemented by subclassing it.
 */
class Allocator
{
public:
    /* Public constants */

    /* Public types */

    /* Public static methods */

    /* Public methods */
    /**
     * \brief Allocates a new object of type T using the active allocator, and
     * return a shared pointer to the object.
     *
     * \note This function is thread-safe.
     *
     * \note May throw any exception thrown by any of T's constructors.
     *
     * \throw std::bad_alloc if memory couldn't be allocated (either for the
     * reference information or the object itself). Note that no garbage
     * collection is performed by New(), even if there is not enough space.
     *
     * \tparam T    the type of the object to create. T must be destructible and
     *              nothrow-move-constructible, i.e. it must have a (possibly
     *              implicit) move or copy constructor that throws no exception
     *              (this means that if T has a custom copy/move constructor, it
     *              must be declared noexcept, which should be the case anyway).
     *              T must be explicitly specified as a template parameter.
     * \tparam Args the types of the arguments. Deduced from the actual argument
     *              list.
     *
     * \param args  the argument list to be forwarded to one of T's constructors.
     */
    template <typename T, typename... Args>
    SharedPtr<T> New(Args &&...args);

    /**
     * \brief Launch a garbage collection.
     *
     * In the single-thread case (\a nb_thread = 1), the garbage collection is
     * immediately launched by the calling thread.
     *
     * In the multi-thread case, the call is blocking until all \a nb_thread
     * threads call this method. At this point, garbage collection is done
     * by one or more calling thread(s). The method returns in all threads only
     * after garbage collection is finished.
     *
     * In both cases, there are two kinds of GC:
     * - a "normal"/minor collection, where only one GC pass is done; it is
     *   faster but uncollected garbage may remain in the allocator
     * - a full/major collection, where as many passes as needed are run to
     *   collect all unreferenced objects
     * This argument may have additional meanings depending on the implementation.
     *
     * \warning Any pointer or reference to an object or its members may be
     * invalidated during the garbage collection; only the shared pointers are
     * guaranteed to be still valid after the method returns.
     *
     * \warning All threads having access to a reference to any object managed
     * by the allocator must call this method (with the appropriate number of
     * threads as an argument), because the garbage collection may modify any
     * reference (if objects are moved). There is no way to have threads access
     * references while garbage collecting (without language/compiler
     * support)...
     */
    void GarbageCollection(bool full = false, unsigned int nb_thread = 1);

    /* Getters, setters */

    /* Constructors, destructor */
    // Abstract class
    Allocator() = delete;

    // Not copiable or movable
    Allocator(const Allocator &) = delete;
    Allocator(Allocator &&) = delete;

    // Derived classes must do a GC when destroyed
    virtual ~Allocator();

    /* Operators */

protected:
    /* Protected constants */

    /* Protected types */
    /**
     * \brief Information about a type that need to be stored at runtime.
     *
     * The function wrappers take void* arguments since the actual type is not
     * known at runtime.
     *
     * This is essentially aimed at classes. For built-in types or classes
     * with trivial operations, certain fields can be empty.
     *
     * \sa type_info_index_
     */
    struct TypeInfo
    {
        /** Wrapper calling the destructor of \a obj. */
        std::function<void(void *obj)> destroy;
        /** Wrapper moving \a obj to \a dest using move semantics (the move-
          * constructor is used if available). */
        std::function<void(void *dest, void *obj)> move;
    };

    /* Protected methods */
    // Constructor for child classes
    Allocator(size_t ref_pool_size, bool mt_opt = false)
        : mem_ref_pool_size_(ref_pool_size),
          mem_ref_pool_(ref_pool_size, MemRefInfo{&uncollected_garbage_}),
          uncollected_garbage_(false), mt_opt_(mt_opt)
    {}

    /**
     * \brief Finds a free reference slot in mem_ref_pool_, set its counter to
     * 0 and return it.
     *
     * To guarantee thread-safety, the counter has to be atomically set to 0
     * when a free slot is found.
     *
     * \throw std::bad_alloc if no slot is available.
     */
    MemRefInfo &getRefSlot();

    /**
     * \brief Utility method to destroy if needed an object in a reference with
     * checking.
     */
    void destroyObject(MemRefInfo &ref);

    /**
     * \brief Utility method to move a block (including an object) to another
     * block.
     *
     * The header and data of \a src is moved to \a dest and the reference
     * is updated.
     *
     * B1 and B2 are supposed to have these members: ref, data and data_size (see
     * e.g. Block in compacting_allocator.cpp).
     *
     * If B1 != B2, then B2 must be contained in B1 or the other way around
     * (the members of B2 must be at the same position in B1).
     *
     * The old and new locations for the block header may overlap, but the
     * old and new locations for the data itself *may not* overlap (copy/move
     * constructors are not supposed to handle such an overlap).
     */
    template <typename B1, typename B2>
    void moveBlock(B1 *dest, B2 *src);

    /**
     * \brief Allocates a block of memory of size \a size, and returns a pointer
     * to its beginning.
     *
     * The allocated block is aligned at least on DATA_ALIGN (see config.h).
     *
     * \throw std::bad_alloc if there is not enough space to allocate the block.
     *
     * \param size  the size of the block to allocate, must be > 0
     * \param ref   a pointer to the reference that will be used to access the
     *              block of memory
     */
    virtual void *allocate(size_t size, MemRefInfo *ref) = 0;

    /**
     * \brief Do a garbage collection cycle with one thread.
     */
    virtual void doGCSingleThread(bool full) = 0;

    /**
     * \brief Do a garbage collection cycle with multiple threads.
     *
     * All the threads involved in GarbageCollection() call this function with
     * a different \a id.
     *
     * By default (if the method is not overriden), the thread of ID 0 does
     * a single thread GC and the other threads do nothing.
     *
     * \param full      see GarbageCollection()
     * \param id        a number assigned to the current thread in 0..nb_thread-1.
     * \param nb_thread the total number of threads.
     */
    virtual void doGCMultiThread(bool full,
                                 unsigned int id, unsigned int nb_thread);


    /* Protected attributes */
    /**
     * \brief The size of the pool of references, i.e. the maximum number of
     * memory references that can be managed (and as a consequence, the maximum
     * number of objects that can be allocated).
     */
    const size_t mem_ref_pool_size_;

    /**
     * \brief The pool of references (every smart pointer created through
     * this allocator is associated to an element of this array).
     *
     * The elements of the pool must have a fixed address, because the smart
     * pointers hold a pointer to them. For this reason, the size of the vector
     * is fixed to MAX_REFS and never modified, otherwise reallocation could
     * happen; an std::set could be used instead but to avoid fragmentation
     * a single block of memory is used.
     *
     * This is conceptually an std::array, but a vector is used because dynamic
     * allocation is preferable given the typical size of the array.
     */
    std::vector<MemRefInfo> mem_ref_pool_;

    /**
     * \brief A marker indicating whether there is uncollected garbage.
     *
     * All references hold a pointer to this variable. Whenever a reference's
     * counter drops to 0, this variable is set to true. The GC then reset
     * it to false.
     *
     * This variable was added to optimise GC: when GC is done, each destructor
     * call may indirectly destroy a shared pointer, decreasing a ref counter.
     * To avoid doing useless passes on the memory pools, the GC method can
     * reset this value before destroying objects; only if it is set to true
     * at the end of the pass is it necessary to do another pass.
     */
    bool uncollected_garbage_;

    /**
     * \brief A mapping between types of managed objects and their associated
     * runtime information.
     *
     * We need to use this cumbersome trick to work around C++'s limited
     * type reflection: once the objects are added to the allocator, we lose
     * their type (they just become a block of memory). This is a problem, since
     * we need to manipulate them during garbage collection (e.g. destroy them).
     * My solution is to make New() store the needed information (actually,
     * operations) in this index whenever a new type is stored.
     *
     * My original idea was to store this information in MemRefInfo, but since
     * all objects of the same type share the same operations it is more
     * efficient to consolidate them in a single index.
     */
    std::unordered_map<std::type_index, TypeInfo> type_info_index_;

    /**
     * \brief The lock used to protect accesses to type_info_index_ where
     * necessary.
     *
     * In particular, it is not needed when garbage collecting because garbage
     * collection is never executed concurrently with any other operation.
     */
    std::mutex type_info_index_lock_;

    /**
     * \brief The barrier used in GarbageCollection() to synchronise multiple
     * threads.
     */
    Util::Barrier gc_barrier_;

    /**
     * \brief Optimisation hint, if true try to optimise for multithread use.
     */
    const bool mt_opt_;
};


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace

// Template methods implementation
#include "allocator.inl"

#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:
