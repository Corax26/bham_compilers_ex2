/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Allocator using a single memory pool but with a moving GC for
 * compaction.
 *
 * The GC compacts the memory pool, so that there is no hole between the blocks
 * (no free list is needed).
 *
 ******************************************************************************
 */

#ifndef GCM_COMPACTING_ALLOCATOR_H_
#define GCM_COMPACTING_ALLOCATOR_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cstdint>
#include <atomic>

/* User */
#include "allocator.h"
#include "Util/array.h"

/* Forward declarations */


namespace GCM
{

/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

/**
 * \brief An allocator using a single memory pool, with a compacting (moving) GC.
 *
 * This is a variant of SimpleAllocator. Instead of leaving the allocated blocks
 * in their place, the garbage collector moves them if space has been reclaimed
 * before them. That way, no free list is necessary since all the free space
 * is located after the allocated blocks, which makes allocate() also quite
 * simple.
 */
class CompactingAllocator : public Allocator
{
public:
    /* Public constants */

    /* Public types */

    /* Public static methods */

    /* Public methods */

    /* Getters, setters */

    /* Constructors, destructor */
    /**
     * \brief Constructs a compacting allocator.
     *
     * The full constructor is used with a reasonable default value for the
     * reference pool size, namely mem_pool_size/DEFAULT_REF_SIZE_RATIO (see
     * config.h)
     */
    explicit CompactingAllocator(size_t mem_pool_size,
                                 bool mt_opt = false);

    /**
     * \brief Constructs a compacting allocator.
     *
     * \param mem_pool_size the total size of the memory pool (aligned up on
     *                      DATA_ALIGN if necessary)
     * \param ref_pool_size the size of the reference pool
     *                      (see Allocator::mem_ref_pool_)
     * \param mt_opt        a hint: if true, try to optimise for multithread use
     */
    CompactingAllocator(size_t mem_pool_size,
                        size_t ref_pool_size,
                        bool mt_opt = false);

    ~CompactingAllocator()
    { GarbageCollection(true); }

    /* Operators */

protected:
    /* Protected constants */

    /* Protected types */

    /* Protected methods */
    void *allocate(size_t size, MemRefInfo *ref) override final;
    void doGCSingleThread(bool full) override final;

    /* Protected attributes */
    /**
     * \brief The memory pool where all the memory blocks are allocated.
     *
     * Its size is fixed to mem_pool_size_.
     */
    Util::Array<uint8_t> mem_pool_;

    /**
     * \brief A pointer to the free zone after the last block in use.
     *
     * If no space is left, points after the end of the memory pool.
     *
     * The pointer is made atomic to enable a thread-safe allocate()
     * implementation.
     */
    std::atomic<uint8_t*> free_zone_;
};


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:

