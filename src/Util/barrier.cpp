/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 *
 ******************************************************************************
 */


/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <mutex>
#include <cassert>

/* User */
#include "barrier.h"

/* Forward declarations */

/* Using clauses */

/******************************************************************************
 *                            Private identifiers                             *
 ******************************************************************************/
namespace
{
/* Types */

/* Functions */

} // Unnamed namespace

namespace Util
{
/******************************************************************************
 *                               Public methods                               *
 ******************************************************************************/

unsigned int Barrier::Wait(unsigned int nb_thread)
{
    // Acquire lock
    std::unique_lock<std::mutex> lock(mutex_);

    // The tag is used to signal that all threads are in the barrier, it is
    // inverted by the last thread. It is also used to ensure that consecutive
    // calls to Wait() don't get mixed up: if the threads call Wait() twice in
    // a row, it will work because the second call will get the inverse tag
    // (if we used fixed values, i.e. initally false then set to true, we would
    // have to reverse it when the last thread of the first call is done and
    // make the second calls wait).
    //
    // Note that an incrementing counter would also work.
    auto tag = call_tag_;

    // Increment counter
    auto id = counter_++;
    assert(id < nb_thread);

    if (counter_ < nb_thread)
    {
        // Wait for the other threads, the last thread will invert call_tag_
        cond_.wait(lock, [=] { return tag == !call_tag_; });
    }
    else
    {
        // This is the last thread
        // Invert the call tag (see above)
        call_tag_ = !call_tag_;
        // Reset counter
        counter_ = 0;
        // Unlock the lock before notifying to avoid useless context switches
        lock.unlock();
        // Wake up the other threads
        cond_.notify_all();
    }

    return id;
}

/******************************************************************************
 *                             Protected methods                              *
 ******************************************************************************/


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace

// vim: expandtab tabstop=4 shiftwidth=4:

