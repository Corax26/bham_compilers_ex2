/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Debug helpers.
 *
 ******************************************************************************
 */

#ifndef UTIL_LOG_H_
#define UTIL_LOG_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <iostream>

/* User */

/* Forward declarations */


/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

class DebugStream : private std::ostream
{
public:
    /* Public constants */

    /* Public types */

    /* Public static methods */

    /* Public methods */

    /* Getters, setters */

    /* Constructors, destructor */
	DebugStream(std::ostream &o)
		: std::ostream(o.rdbuf())
	{
		tie(&o);
	}

    /* Operators */
	template <typename T>
	DebugStream &operator<<(T &&t)
	{
#ifndef NDEBUG
        // Note: this (kind of ugly) trick is needed to use the right
        // overloading of <<, calling std::ostream::operator<<() doesn't do
        // the trick because << for ostream is also overloaded by global 
        // functions
        std::ostream &tmp = static_cast<std::ostream &>(*this);
        tmp << t;
#endif
		return *this;
	}

	// Need to be explicit for this one
	DebugStream &operator<<(std::ostream &(*pf)(std::ostream &))
	{
#ifndef NDEBUG
        std::ostream &tmp = static_cast<std::ostream &>(*this);
        tmp << pf;
#endif
		return *this;
	}

protected:
    /* Protected constants */

    /* Protected types */

    /* Protected methods */

    /* Protected attributes */
};

/**
 * \brief Debug stream.
 *
 * Using << with cdebug only outputs the text to stdout if NDEBUG is not defined.
 */
extern DebugStream cdebug;


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:

