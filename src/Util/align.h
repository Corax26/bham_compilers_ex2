/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Helpers for address/size alignment.
 *
 ******************************************************************************
 */

#ifndef UTIL_ALIGN_H_
#define UTIL_ALIGN_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cstddef>

/* User */

/* Forward declarations */


namespace Util
{
/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/

constexpr size_t AlignUpSize(size_t size, size_t align)
{
    return ((size - 1) / align + 1) * align;
}


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:

