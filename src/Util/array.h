/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   Light wrappers around an array of fixed (runtime) size.
 *
 * Meant to be used as a memory pool, allocated on the heap.
 *
 ******************************************************************************
 */

#ifndef UTIL_ARRAY_H_
#define UTIL_ARRAY_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <cstdint>
#include <cstddef>
#include <cassert>
#include <utility>

/* User */

/* Forward declarations */


namespace Util
{

/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

template <typename T>
struct Array
{
	T * const data;
	const size_t size;

	explicit Array(size_t size)
		: data(new T[size]), size(size)
	{}

	// Not copiable or movable
	Array(const Array &) = delete;
	Array(Array &&) = delete;

	~Array()
	{ delete data; }
};


template <typename T>
struct SwappableArray
{
	T *data;
	const size_t size;

	explicit SwappableArray(size_t size)
		: data(new T[size]), size(size)
	{}

	// Not copiable or movable
	SwappableArray(const SwappableArray &) = delete;
	SwappableArray(SwappableArray &&) = delete;

	~SwappableArray()
	{ delete data; }

    friend void swap(SwappableArray<T> &lhs, SwappableArray<T> &rhs)
    {
        assert(lhs.size == rhs.size);
        using std::swap;
        swap(lhs.data, rhs.data);
    }
};

/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:

