/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   A barrier synchronisation primitive based on C++11's primitives.
 *
 ******************************************************************************
 */

#ifndef UTIL_BARRIER_H_
#define UTIL_BARRIER_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <condition_variable>

/* User */

/* Forward declarations */


namespace Util
{

/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

class Barrier
{
public:
    /* Public constants */

    /* Public types */

    /* Public static methods */

    /* Public methods */
	/**
	 * \brief Wait until \a nb_thread call this method.
	 *
	 * \return a different number in 0..nb_thread-1 for each calling thread.
	 */
	unsigned int Wait(unsigned int nb_thread);

    /* Getters, setters */

    /* Constructors, destructor */
	Barrier()
        : counter_(0)
    {}

    /* Operators */

protected:
    /* Protected constants */

    /* Protected types */

    /* Protected methods */

    /* Protected attributes */
	std::mutex mutex_;
	std::condition_variable cond_;
	unsigned int counter_;
    bool call_tag_;
};


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:
