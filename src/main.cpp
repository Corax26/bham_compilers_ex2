/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   A demonstration of the GCM allocators and smart pointers.
 *
 ******************************************************************************
 */


/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */
#include <memory>
#include <iostream>
#include <vector>
#include <thread>
#include <algorithm>
#include <functional>
#include <sstream>

#include <getopt.h>

/* User */
#include "GCM/shared_ptr.h"
#include "GCM/weak_ptr.h"
#include "GCM/simple_allocator.h"
#include "GCM/compacting_allocator.h"
#include "GCM/hotspot_allocator.h"

/* Forward declarations */

/* Using clauses */
namespace ph = std::placeholders;

/******************************************************************************
 *                            Private identifiers                             *
 ******************************************************************************/
namespace
{
/* Types */
struct Container
{
    // To make a bigger object (twice as big as an int after alignment)
    uint8_t padding[16];
    GCM::SharedPtr<int> curr;
    GCM::SharedPtr<int> prev;

    Container(const GCM::SharedPtr<int> &curr, const GCM::SharedPtr<int> &prev)
        : curr(curr), prev(prev)
    {}
};

struct LinkedList
{
    int data;
    GCM::SharedPtr<LinkedList> next;
    GCM::WeakPtr<LinkedList> prev;

    LinkedList(int data, const GCM::SharedPtr<LinkedList> &next = {},
                         const GCM::WeakPtr<LinkedList> &prev = {})
        : data(data), next(next), prev(prev)
    {}
};

/* Globals */
std::unique_ptr<GCM::Allocator> Alloc;

std::string TestName;
size_t TestIter;
size_t NbThread = 1;

/* Functions */
void usage(const char *name)
{
    std::cout
        << "Usage: " << name << " OPTIONS <test_name> <nb_iter>\n"
        << "Available tests: simple_loop, memory_loop, linked_list, linked_list_reverse\n"
        << "Required options:\n"
        << "-a, --alloc=            the allocator: simple, compacting or hotspot\n"
        << "-s, --pool_size=        the total pool size for the allocator\n"
        << "Options for SimpleAllocator (simple):\n"
        << "-A, --alloc_algo=       the allocation algorithm: first, best or next (meaning first fit, etc.) (default: first)\n"
        << "Options for HotspotAllocator (hotspot):\n"
        << "-r, --new_ratio=        the new ratio (default: " << GCM::HotspotAllocator::DEFAULT_NEW_RATIO << ")\n"
        << "-R, --survivor_ratio=   the survivor ratio (default: " << GCM::HotspotAllocator::DEFAULT_SURVIVOR_RATIO << ")\n"
        << "-T, --age_threshold=    the age threshold to tenure objects in the young generation (default: " << GCM::HotspotAllocator::DEFAULT_AGE_THRESHOLD << ")\n"
        << "Test options:\n"
        << "-t, --nb_thread=        the number of threads to use (default: 1)\n"
        << std::flush;
    exit(2);
}

void parseArgs(int argc, char * const argv[])
{
    static struct option long_opts[] = {
        { "alloc",          required_argument, 0, 'a' },
        { "pool_size",      required_argument, 0, 's' },
        { "alloc_algo",     required_argument, 0, 'A' },
        { "new_ratio",      required_argument, 0, 'r' },
        { "survivor_ratio", required_argument, 0, 'R' },
        { "age_threshold",  required_argument, 0, 'T' },
        { "nb_threads",     required_argument, 0, 't' },
        { nullptr, 0, 0, 0 }
    };

    bool done = false;
    char alloc = '\0';
    size_t pool_size = 0;
    auto alloc_algo = GCM::SimpleAllocator::FIRST_FIT;
    auto new_ratio = GCM::HotspotAllocator::DEFAULT_NEW_RATIO;
    auto survivor_ratio = GCM::HotspotAllocator::DEFAULT_SURVIVOR_RATIO;
    auto age_threshold = GCM::HotspotAllocator::DEFAULT_AGE_THRESHOLD;
    try
    {
        while (!done)
        {
            auto res = getopt_long(argc, argv, "a:s:A:r:R:T:t:", long_opts, nullptr);
            std::string arg;
            if (optarg) arg = optarg;
            switch (res)
            {
                case 'a':
                    if (arg == "simple") alloc = 's';
                    else if (arg == "compacting") alloc = 'c';
                    else if (arg == "hotspot") alloc = 'h';
                    else throw std::invalid_argument{""};
                    break;
                case 's':
                    pool_size = std::stoul(arg);
                    break;
                case 'A':
                    if (arg == "first")
                        alloc_algo = GCM::SimpleAllocator::FIRST_FIT;
                    else if (arg == "best")
                        alloc_algo = GCM::SimpleAllocator::BEST_FIT;
                    else if (arg == "next")
                        alloc_algo = GCM::SimpleAllocator::NEXT_FIT;
                    else throw std::invalid_argument{""};
                    break;
                case 'r':
                    new_ratio = std::stoul(arg);
                    break;
                case 'R':
                    survivor_ratio = std::stoul(arg);
                    break;
                case 'T':
                    age_threshold = std::stoul(arg);
                    break;
                case 't':
                    NbThread = std::stoul(arg);
                    break;
                case -1:
                    done = true;
                    break;
                default:
                    throw std::invalid_argument{""};
            }
        }

        if (new_ratio == 0 || survivor_ratio == 0 || NbThread == 0)
            throw std::invalid_argument{""};

        while (optind < argc)
        {
            if (TestName.empty()) TestName = argv[optind++];
            else
            {
                TestIter = std::stoul(argv[optind++]);
                if (argc != optind) throw std::invalid_argument{""};
            }
        }
    }
    catch (std::invalid_argument &)
    {
        usage(argv[0]);
    }

    GCM::Allocator *new_alloc = nullptr;
    switch (alloc)
    {
        case 's':
            new_alloc = new GCM::SimpleAllocator{pool_size, alloc_algo};
            break;
        case 'c':
            new_alloc = new GCM::CompactingAllocator{pool_size};
            break;
        case 'h':
            new_alloc = new GCM::HotspotAllocator
                {pool_size, new_ratio, survivor_ratio, age_threshold};
            break;
        default:
            usage(argv[0]);
    }

    Alloc.reset(new_alloc);
}

/*
 * Test helpers
 */

void runThreads(size_t nb_threads,
                const std::function<void(size_t thread_id)> &func)
{
    std::vector<std::thread> threads;

    // Start threads
    for (size_t id = 0; id < NbThread; ++id)
    {
        threads.emplace_back(func, id);
    }

    // Join threads
    std::for_each(threads.begin(), threads.end(),
                  std::bind(&std::thread::join, ph::_1));
}

/*
 * Loop tests
 */

void testLoopThread(size_t thread_id, bool memory)
{
    // Only output the list, the rest would be interleaved anyway
    std::vector<GCM::SharedPtr<int>> list;
    GCM::SharedPtr<int> prev;
    std::stringstream stream;
    for (size_t i = thread_id * TestIter; i < (thread_id + 1) * TestIter; ++i)
    {
        // In case alloc fails
        static bool stop = false;
        stream << "Thread " << thread_id << " - Iter " << i << "\n";

        try
        {
            auto curr = Alloc->New<int>(i);
            auto cont = Alloc->New<Container>(curr, prev);
            if (memory) list.push_back(curr);
            prev = curr;
            stream << "Curr: " << *cont->curr << ", prev: "
                   << (cont->prev ? std::to_string(*cont->prev) : "-") << "\n";
        }
        catch (std::bad_alloc &)
        {
            stream << "Out of memory\n";
            stop = true;
        }

        bool do_gc = i % 2 || stop;
        if (do_gc && thread_id == 0) stream << "Starting garbage collection\n";

        stream >> std::cout.rdbuf(); std::cout.flush();
        stream.clear();

        // Synchronised garbage collection
        if (do_gc) Alloc->GarbageCollection(false, NbThread);

        if (stop) break;
    }

    if (memory)
    {
        stream << "Thread " << thread_id << " - List:";
        for (const auto &ref : list)
        {
            stream << " " << *ref;
        }
        stream << "\n";
        stream >> std::cout.rdbuf(); std::cout.flush();
    }
}

void testLoop(bool memory)
{
    if (NbThread > 1)
    {
        runThreads(NbThread, std::bind(&testLoopThread, ph::_1, memory));
        return;
    }

    std::vector<GCM::SharedPtr<int>> list;
    GCM::SharedPtr<int> prev;
    for (size_t i = 0; i < TestIter; ++i)
    {
        std::cout << "\nIter " << i << std::endl;
        try
        {
            auto curr = Alloc->New<int>(i);
            auto cont = Alloc->New<Container>(curr, prev);
            if (memory) list.push_back(curr);
            prev = curr;
            std::cout << "Curr: " << *cont->curr
                << ", prev: "
                << (cont->prev ? std::to_string(*cont->prev) : "-")
                << std::endl;
        }
        catch (std::bad_alloc &)
        {
            std::cout << "Out of memory" << std::endl;
            break;
        }

        if (i % 2)
        {
            std::cout << "Starting garbage collection" << std::endl;
            Alloc->GarbageCollection();
        }
    }

    if (memory)
    {
        std::cout << "List:";
        for (const auto &ref : list)
        {
            std::cout << " " << *ref;
        }
        std::cout << std::endl;
    }
}

/*
 * Linked list tests
 */

void testLinkedListThread(size_t thread_id, bool reverse)
{
    size_t i = thread_id * TestIter;
    // The head is not actually used, but if we don't use it the function is
    // not valid since the first element would have its ref counter drop to 0
    // before being used
    GCM::SharedPtr<LinkedList> head, tail;
    if (!reverse) head = Alloc->New<LinkedList>(i++);
    else          tail = Alloc->New<LinkedList>(i++);

    if (!reverse)
    {
        auto curr = head;
        while (i < (thread_id + 1) * TestIter)
        {
            try
            {
                auto next = Alloc->New<LinkedList>(i++);
                next->prev = curr;
                curr->next = next;
                curr = next;
            }
            catch (std::bad_alloc &)
            {
                std::cout << "Out of memory\n" << std::flush;
                break;
            }
        }
        tail = curr;
    }
    else
    {
        auto curr = tail;
        while (i < TestIter)
        {
            auto prev = Alloc->New<LinkedList>(i++);
            prev->next = curr;
            curr->prev = prev;
            curr = prev;
        }
        head = curr;
    }

    std::stringstream stream;
    if (NbThread > 1) stream << "Thread " << thread_id << " - List (backwards):";
    else              stream << "List (backwards):";
    // Use a weak pointer as iterator, because a weak pointer can be assigned
    // a shared pointer, but not the other way round (also it avoids modifying
    // the ref counters)
    GCM::WeakPtr<LinkedList> it = tail;
    while (it)
    {
        stream << " " << it->data;
        it = it->prev;
    }
    stream << "\n";
    stream >> std::cout.rdbuf(); std::cout.flush();

    // No explicit GC, because it would collect nothing as long as head is alive
    // (it will be done when the allocator is destroyed)
}

void testLinkedList(bool reverse)
{
    if (NbThread > 1)
        runThreads(NbThread, std::bind(&testLinkedListThread, ph::_1, reverse));
    else
        testLinkedListThread(0, reverse);
}

} // Unnamed namespace

/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/

int main(int argc, char * const argv[])
{
    parseArgs(argc, argv);

    if (TestName == "simple_loop")
        testLoop(false);
    else if (TestName == "memory_loop")
        testLoop(true);
    else if (TestName == "linked_list")
        testLinkedList(false);
    else if (TestName == "linked_list_reverse")
        testLinkedList(true);
    else usage(argv[0]);

    return 0;
}

// vim: expandtab tabstop=4 shiftwidth=4:
