SRCDIR		= ./src
BUILDDIR	= ./build
EXE			= alloc_demo
CXX_FILES 	= \
	GCM/allocator.cpp \
	GCM/simple_allocator.cpp \
	GCM/compacting_allocator.cpp \
	GCM/hotspot_allocator.cpp \
	GCM/mem_ref.cpp \
	Util/barrier.cpp \
	Util/log.cpp \
	main.cpp

CXX			?= g++
LD			= $(CXX)
# -MMD generates a .d dependency file alongside the .o file
# Disable -Wunused-parameter because flexc++ generated files
# generate warning (and this warning is not very useful anyway)
CXXFLAGS 	= -std=c++11 \
			  -Wall -Wextra -Wno-unused-parameter \
			  -I$(SRCDIR) \
			  -pthread \
			  -MMD
LDFLAGS		= -pthread
LDLIBS 		=

OBJS += $(patsubst %.cpp,$(BUILDDIR)/%.o, $(CXX_FILES))

.PHONY: all debug release clean

all:			debug
debug: 			build
release: 		build
prof:			build
build: 			pre-build $(EXE)

# Target-specific variables
debug:			CXXFLAGS += -O0 -ggdb
release: 		CXXFLAGS += -O3 -g0 -DNDEBUG
prof:			CXXFLAGS += -O3 -DNDEBUG -pg
prof:			LDFLAGS  += -pg

pre-build:
	@# Creating build and executable path if necessary
	@mkdir -p $(BUILDDIR)
	@mkdir -p $(BUILDDIR)/GCM
	@mkdir -p $(BUILDDIR)/Util

clean:
	find $(BUILDDIR) \( -name '*.o' -or -name '*.d' \) -delete
	$(RM) $(EXE)

$(EXE): $(OBJS)
	$(LD) $(LDFLAGS) -o $(EXE) $(OBJS) $(LDLIBS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# Use generated dependency files
-include $(OBJS:%.o=%.d)
