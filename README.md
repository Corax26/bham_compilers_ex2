# A Generic C++ Garbage Collector Library

## Introduction & overview

This project, carried out as part of the Compilers & Languages Masters module at the University of Birmingham, aims at "emulating" a garbage collector as a generic C++ library (allocation of objects of arbitrary type), with several allocators/garbage collectors implementation (and an easy way to add other implementations). Before going any further, let's make something clear: C++ is not made to be garbage-collected, and a mere library cannot efficiently implement such a mechanism. The goal of this project is rather to highlight the workings of a garbage collector and show that it is possible to implement one in C++, with several limitations due to the lack of language/compiler support.

Implementing a garbage collected allocator in pure C++ presents two main issues:

1. We need to know if an allocated object has become garbage, i.e. if it is still in use. In a natively garbage-collected language like Java, the GC can walk the whole object tree, including those referenced on the stack, but no such thing is possible without direct support from the compiler.
2. For moving GCs, we have to modify the location of objects while they are still in use. For the same reason as above, this is impossible without compiler support, since we don't know where the objects are referenced.

To circumvent these issues, I chose to use two mechanisms:

1. For the issue #1, we need to control how the objects are referenced. This is achieved by using smart pointers and reference counting: an object is in use as long as shared pointers to an object are still alive (i.e. the reference counter is non-zero). By using this interface, we don't need anymore to find all the references throughout the program.
2. The regular implementation of smart pointers would not solve the issue #2, because the smart pointers directly store a pointer to the object (and a pointer to the reference count); since the allocator doesn't have access to the smart pointers it would be impossible to move objects without invalidating the smart pointers. My solution is to put both the pointer to the object and the reference count in a structure (MemRefInfo) which is stored by the allocator, and have the smart pointers store a pointer to this structure. We can now move objects without invalidating smart pointers (only the MemRefInfo structure needs to be updated), at the price of an additional indirection.

The architecture of the library is quite simple, with two main classes (see the simplified class diagram in `class_diagram.svg`):

- SharedPtr: the modified shared pointer implementation, storing a pointer to a MemRefInfo struct as explained before. Since reference counting is used, care must be taken to avoid cycles, for this reason I also provide a WeakPtr class to break cycles. The main difference with a "classic" shared pointer implementation is that when the reference counter drops to 0 (when the last shared pointer is destroyed), the destruction of the object is not done immediately but delayed until the GC is run (see `ref_counter_lifecycle.pdf`).
- Allocator: the abstract class managing allocated objects. The base Allocator class stores the MemRefInfo structures and provides a New() method returning a shared pointer to a new allocated object, and a GarbageCollection() method to run the garbage collector. However, the storage of the data is delegated to subclasses which must implement the allocate() method (called by New()) and the doGCSingleThread() method (called by GarbageCollection()).

I provide three different allocator implementations, subclasses of Allocator:
- SimpleAllocator, an allocator with one memory pool and a non-moving GC, which allocates and frees memory in a similar fashion to heap allocators (malloc()).
- CompactingAllocator, an allocator with one memory pool but a moving GC, which compacts the pool during GC so that there is no free hole in the pool.
- HotspotAllocator, an allocator based on the HotSpot JVM allocators with several pools and a moving GC.

There is no allocator where the used blocks are arranged in a tree, because it felt rather weird in my setup.

### Memory pools

Each allocator implementation uses its own memory pool(s). In all cases, these pools are allocated on the heap (because they're much too large to be allocated on the stack), but their size is fixed at the construction of the allocator. For non-moving GCs like SimpleAllocator, it would be very difficult to have variable size pools since we would have to actually have a variable number of pools since objects must not be moved, forbidding the use of realloc(). For moving GCs, it would be possible to use realloc() and move all the objects in the resized pool, but it generates an additional complexity that didn't seem worth it to me in this project.

### Garbage collection

Using reference counting to know which objects are in use has an unexpected and unfortunate effect on garbage collection. Indeed, when an object managed by an allocator reference another object in this allocator (through a shared pointer), the reference counter of the latter will not drop to 0 before the former has been destroyed. As a consequence, depending on the order of destruction of objects during GC, there may be remaining garbage after a GC cycle is completed. To work around that, I added a flag in the allocator which is set whenever a reference counter drops to 0. That way, it is possible to know if additional objects have become unused during a GC cycle (i.e. doGCSingle/MultiThread()), and there is two modes of operation for GarbageCollection(): the "normal" one where only one cycle is done, and the "full" one where cycles are done as long as the flag is set during a cycle (and also triggers a major collection in the case of the hotspot allocator). Clearly, this can be very suboptimal in certain cases, but I don't think there is another solution when using reference counting.

However, using the double indirection described previously has another unexpected effect which makes the implementation of the moving GCs much easier: since managed objects never directly reference other objects, there is no need to update other objects when moving an object (for instance, we don't need forwarding pointers for the hotspot allocator). It feels a bit like cheating to be honest, but there is no safe way in C++ to update pointers in an arbitrary object anyway since C++ is limited in terms of reflection (there is no way to generically inspect members of a class).

### Multithread usage

Both the smart pointers and the allocators are thread-safe (Allocator::New() can be called concurrently, and different threads can use shared pointers to the same object). However, there is an important limitation: the garbage collection has to be run by all threads at the same time. Indeed, for moving GCs, we need a stop-the-world collection, and we can't just pause the threads before starting a GC cycle (they may be currently accessing managed objects that will potentially be moved). Consequently, all the threads must regularly call GarbageCollection(), which waits for all the threads to call it before starting the actual collection.

Additionally, garbage collection can be done by multiple threads, subclasses just have to override the doGCMultiThread() method. This would definitely be possible for the hotspot allocator, but I didn't have time to actually implement it (the default implementation falls back to doGCSingleThread()).

### Documentation

All the headers are completely documented using Doxygen comments. Since some headers are quite big, it may be more convenient to read the generated Doxygen documentation (in HTML), for this reason I provide a Doxyfile to generate it (just run `doxygen Doxyfile` and open `doxygen/html/index.html`, you only need Doxygen and GraphViz (specifically `dot`)).


## Testing and demonstration

I used assertions extensively, and it proved quite useful. The most important one is during the destruction of Allocator: subclasses must trigger a full garbage collection when being destroyed, as a consequence no object must still be allocated when Allocator is destroyed. If this assertion doesn't fail, it shows that all the objects were still reachable by the allocator and memory has been properly released.

### Compilation

A fairly recent C++11 compiler is needed. I used gcc 4.9.2, and you need at least gcc 4.8 because I rely on C++11 features implemented rather recently. It compiles fine with clang 3.5 too (pass an additional `CXX=clang++` to `make`), clang 3.3+ should also work.

By default, `make` compiles in debug mode, which means without optimisation but with all the assertions and debug messages. You can also compile in release mode with `make release` which does the opposite (optimises and suppresses assertions and debug messages), don't forget to `make clean` before changing mode.

### Demonstration

I provide small demonstration tests in `main.cpp`. Invoking the program (`alloc_demo`) without argument shows the available options. You need to specify which allocator to use and the size of the memory pool(s) (total size in the case of the hotspot allocator), and there are also other options for certain allocators. You can also choose to run the test on several threads. The required positional arguments are the name of the test and the number of iterations to do / number of elements to add.

I provide two small tests, with one variant each:

- `simple_loop`: int's and structures containing shared pointers to the ints of the previous and current iteration are repeatedly allocated. Garbage collection is run every two iterations, collecting everything but the int referenced by the `prev` shared pointer. A small pool size is enough (no matter how many iterations are done) since objects are not saved more than two iterations.
- `memory_loop`: just like `simple_loop`, but shared pointers to the int's are kept in a vector, and are all printed at the end. Therefore, only the temporary structures are collected during the GCs. The needed pool size is proportional to the number of iterations.
- `linked_list`: a doubly-linked list is created and then printed backwards. This shows the usage of weak pointer (the `prev` member is a weak pointer, otherwise there would be cycles everywhere). Garbage collection is not explicitly run because there would be nothing to collect, but a full collection is done as usual when the allocator is destroyed.
- `reverse_linked_list`: just like `linked_list`, but the list is created backwards (starting from the tail), inverting the indices. The interesting part is that in that case, the final full collection takes as many cycles to complete as there are elements in the linked list. This is a consequence of the GC mechanism explained before: since the garbage collectors walk the object list in the same order they've been allocated (when the pool is not too full), if objects refer to objects before them, then the objects before them will not be freed in the same cycle they are freed. This situation is the worst case, doing normal collections will only free one object even if all the list has become unused.

To observe how the allocators work, you should compile the program in debug mode (the default) to get the debug messages (allocation, moving and destruction of objects) and run tests with a small number of iterations, e.g. `./alloc_demo -a simple -s 512 simple_loop 8`. Regarding debug messages, note that the sizes relate to the blocks, i.e. both the header and the data, both being aligned to the minimum alignment requirements (16B on x64).

Regarding the pool size, note that the hotspot allocator needs a bigger size since the space is split into several pools. Modifying ratios (in particular the new ratio) can have a dramatic effect on the needed pool size.

Running the tests on multiple threads can be interesting to add randomness in the allocation.
