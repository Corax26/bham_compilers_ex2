/**
 ******************************************************************************
 *                    Compilers & Languages - Assignment 2
 *                             Garbage collectors
 *                            (c) 2014 K. Brodsky
 * \file
 * \brief   <brief>
 *
 ******************************************************************************
 */

#ifndef <PATH_FILENAME>_H_
#define <PATH_FILENAME>_H_

/******************************************************************************
 *                              Included headers                              *
 ******************************************************************************/

/* System */

/* User */

/* Forward declarations */


namespace <Namespace>
{

/******************************************************************************
 *                                  Classes                                   *
 ******************************************************************************/

class <Class>
{
public:
    /* Public constants */

    /* Public types */

    /* Public static methods */

    /* Public methods */

    /* Getters, setters */

    /* Constructors, destructor */

    /* Operators */

protected:
    /* Protected constants */

    /* Protected types */

    /* Protected methods */

    /* Protected attributes */

};


/******************************************************************************
 *                              Global functions                              *
 ******************************************************************************/


} // Namespace
#endif // Include guard

// vim: expandtab tabstop=4 shiftwidth=4:
